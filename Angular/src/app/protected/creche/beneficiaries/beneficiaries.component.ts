import { Component } from '@angular/core';
import { AllService } from '../../services/all.service';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-beneficiaries-creche',
  templateUrl: './beneficiaries.component.html'
})
export class BeneficiariesComponent {
  beneficiaries: any;
  hayError: boolean = false;
  data: any=[];
  header = ['Id', 'Niño/a', 'Fecha Nacimiento','Edad', 'Estado', 'Grado', 'Sala'];

  constructor(private allService: AllService, private http: HttpClient) { }

  ngOnInit(): void {
    this.initTable();
  }

  // Función para manejar el cambio de página
  onPageChange(data: any): void {
    const url = `${data}`;
    const res = this.http.get(url);
    res.subscribe({
      next: (response) => {
        this.data = response;
      }
    })
  }

  recargarDatosTabla() {
    this.initTable();
  }

  private initTable() {
    this.allService.indexBeneficiary().subscribe({
      next: (beneficiaries) => {
        this.beneficiaries = beneficiaries;
        this.data = this.beneficiaries;
      }, error: () => {
        this.hayError = true;
      }
    });
  }
}
