import { Component } from '@angular/core';
import { AllService } from '../../services/all.service';
import { HttpClient } from '@angular/common/http';
import { Requests, RequestsResponse } from '../../interfaces/requests-interface';

@Component({
  selector: 'app-requests-creche',
  templateUrl: './requests.component.html'
})
export class RequestsComponent {

  request: Requests;
  hayError: boolean = false;
  data: any = [];
  // header = ['Id','Folio', 'Beneficiario', 'Edad', 'Tutor', 'Prioridad', 'Grado', 'Fecha', 'Estado'];
  header = ['No.','Folio', 'Niño/a', 'Edad', 'Trabajador', 'Fecha', 'Estado'];


  constructor(private allService: AllService, private http: HttpClient) { }

  ngOnInit(): void {
    this.initTable();
  }

  recargarDatosTabla() {
    this.initTable();
  }

  // Función para manejar el cambio de página
  onPageChange(data: any): void {
    const url = `${data}`;
    const res = this.http.get(url);
    res.subscribe({
      next: (response) => {
        this.data = response;
      }
    })
  }

  private initTable() {
    this.allService.indexRequest().subscribe({
      next: (request) => {
        this.request = request;
        this.data = this.request;
        console.log(request);
      }, error: () => {
        this.hayError = true;
      }
    });
  }
}
