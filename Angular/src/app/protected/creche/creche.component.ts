import { Component } from '@angular/core';

@Component({
  selector: 'app-creche',
  templateUrl: './creche.component.html'
})
export class CrecheComponent {
  opciones = [
    // { icon: 'bi bi-grid', title: 'Dashboard', route: '/admin/guarderia/dashboard' },
    { icon: 'bi bi-file-earmark-zip', title: 'Solicitudes', route: '/admin/guarderia/solicitudes' },
    { icon: 'bi bi-calendar-range', title: 'Citas', route: '/admin/guarderia/citas' },
    { icon: 'bi bi-person-fill', title: 'Beneficiarios', route: '/admin/guarderia/beneficiarios' },
    { icon: 'bi bi-house', title: 'Salas', route: '/admin/guarderia/salas/' }
  ];
  urlLogo = '/admin/guarderia/dashboard';
}
