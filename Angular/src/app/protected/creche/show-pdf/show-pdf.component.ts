import { Component } from '@angular/core';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { CrecheService } from '../../services/creche.service';
import { environment } from 'src/environments/environment';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-show-pdf',
  templateUrl: './show-pdf.component.html',
  styleUrls: ['./show-pdf.component.css']
})
export class ShowPdfComponent {
  pdfUrl: SafeResourceUrl;
  baseUrl: string = environment.baseUrl;


  constructor(private route: ActivatedRoute,private sanitizer: DomSanitizer,private crecheService:CrecheService){}
  
  ngOnInit(): void {
    this.route.params.subscribe(params => {
      const id = params['id'];
      if (id) {
        const unsafeUrl = this.baseUrl+'/creche/generar-pdf/'+id;
        this.pdfUrl = this.sanitizer.bypassSecurityTrustResourceUrl(unsafeUrl);
      }})
  }
}
