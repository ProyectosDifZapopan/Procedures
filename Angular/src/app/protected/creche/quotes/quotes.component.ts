import { Component } from '@angular/core';
import { AllService } from '../../services/all.service';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-quotes-creche',
  templateUrl: './quotes.component.html'
})
export class QuotesComponent {
  quotes: any;
  hayError: boolean=false;
  data: any=[];
  // header=['Id','Beneficiario','Prioridad','Grado','Fecha','Horario'];
  header=['No.','Folio','Niño/a','Trabajador','Fecha','Horario'];


  constructor(private allService:AllService,private http: HttpClient){}

  ngOnInit(): void {
    this.initTable();
  }

  recargarDatosTabla() {
    this.initTable();
  }

    // Función para manejar el cambio de página
    onPageChange(data: any): void {
      const url = `${data}`;
      const res = this.http.get(url);
      res.subscribe({
        next: (response) => {
          this.data = response;
        }
      })
    }

  private initTable(){
    this.allService.indexQuote().subscribe({next:(quotes)=>{
        this.quotes=quotes;

        this.data = this.quotes;
      },error:()=>{
        this.hayError=true;
      }
    });
  }
}
