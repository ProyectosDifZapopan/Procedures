import { Component, ElementRef } from '@angular/core';
import { CrecheService } from '../../services/creche.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-care-center',
  templateUrl: './care-center.component.html'
})
export class CareCenterComponent {
  creches: any;
  hayError: boolean = false;
  data: any[];
  headers = ['Id', 'Grado', 'Sala', 'Capacidad', 'Activos', 'Libre(s)'];
  beneficiaries: any = false;
  beneficiary: any = false;

  processDegree: any[] = []; // Replace with your actual data
  refuseDegree: any[] = []; // Replace with your actual data
  beneficiariesDegree: any[] = []; // Replace with your actual data
  capacityDegree: any[] = []; // Replace with your actual data
  processedDataDegree: any[] = [];

  constructor(private elementRef: ElementRef, private fb: FormBuilder, private crecheService: CrecheService) { }

  miFormulario: FormGroup = this.fb.group({
    creche_id: [[Validators.nullValidator]],
  });


  ngOnInit(): void {
    this.initTable();
  }

  private initTable() {
    this.crecheService.indexCreche().subscribe({
      next: (response) => {
        this.creches = response.creches;
        this.data = this.creches.data;
        this.processDegree = response.degree.process;
        this.refuseDegree = response.degree.refuse;
        this.beneficiariesDegree = response.degree.benediciaries;
        this.capacityDegree = response.degree.capacity;
      }, error: () => {
        this.hayError = true;
      }
    });
  }

  setBeneficiary(data: any) {
    this.miFormulario.patchValue({
      creche_id: data.beneficiary_creche[0].creche_id
    });
    this.beneficiary = data;
  }

  changeStatusBeneficiary(status: number, pivote_id: number) {
    Swal.fire({
      position: 'center',
      icon: 'question',
      title: '¿Está seguro de que desea ' + (status == 0 ? 'dar de baja' : (status == 1 ? 'dar de alta' : '')) + ' a este usuario de la sala?',
      showConfirmButton: true,
      showCancelButton: true,
      confirmButtonText: 'Si',
      cancelButtonText: `No`
    }).then((result) => {
      if (result.isConfirmed) {
        const body = {}
        body['pivote_id'] = pivote_id;
        body['status'] = status;
        this.crecheService.updateBeneficiaryCreche(body).subscribe({
          next: (response) => {
            if (response.code == 200) {
              Swal.fire({
                position: 'center',
                icon: 'success',
                title: response.message,
                showConfirmButton: false,
                timer: 2500
              })
              this.initTable();
            } else {
              Swal.fire({
                position: 'center',
                icon: 'error',
                title: response.message,
                showConfirmButton: false,
                timer: 2500
              })
            }
          }, error: (error) => {
            Swal.fire("Error", "error");
          }
        })
      } else if (result.isDenied) {
        return;
      }
    })


  }

  getBeneficiaries(creche_id: number) {
    this.crecheService.showBeneficiaryCreche(creche_id).subscribe({
      next: (beneficiary) => {
        this.beneficiaries = beneficiary;
      }, error: () => {
        this.hayError = true;
      }
    });
  }

  changeCreche() {
    const form = this.miFormulario.value;
    const [id, capacity, name] = form.creche_id.split(',');
    if (form == 0) {
      return;
    }
    const data = {}
    data['pivote_id'] = this.beneficiary.beneficiary_creche[0].id;
    data['creche_id'] = id;
    data['quota'] = capacity;
    data['beneficiary_id'] = this.beneficiary.id;


    Swal.fire({
      position: 'center',
      icon: 'question',
      title: '¿Está seguro de que desea cambiar a este usuario a la sala ' + name + ' ?',
      showConfirmButton: true,
      showCancelButton: true,
      confirmButtonText: 'Si',
      cancelButtonText: `No`
    }).then((result) => {
      if (result.isConfirmed) {
        this.crecheService.updateBeneficiaryCreche(data).subscribe({
          next: (response) => {
            if (response.code == 200) {
              Swal.fire({
                position: 'center',
                icon: 'success',
                title: response.message,
                showConfirmButton: false,
                timer: 2000
              })
              this.initTable();
            } else {
              Swal.fire({
                position: 'center',
                icon: 'error',
                title: response.message,
                showConfirmButton: false,
                timer: 2000
              })
            }
          }, error: (error) => {
            Swal.fire("Error", "error");
          }
        })
      } else if (result.isDenied) {
        return;
      }
    })
  }
}
