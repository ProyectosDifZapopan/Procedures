import { Component, ElementRef, EventEmitter, Input, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Subject, debounceTime } from 'rxjs';
import { AllService } from 'src/app/protected/services/all.service';
import { CrecheService } from 'src/app/protected/services/creche.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-quotes',
  templateUrl: './quotes.component.html'
})
export class QuotesComponent {
  @Input() headers: any = [];
  @Input() data: any = [];
  @Output() asistenciaCreada: EventEmitter<any> = new EventEmitter<any>();
  @Output() pageChange: EventEmitter<string> = new EventEmitter<string>();
  onPageChange(url: string): void {
    // Emitir el evento al componente padre
    this.pageChange.emit(url);
  }

  id: any = null;
  creches: any = null;
  beneficiary: any = null;
  quote_id: any = null;
  request_id: any = null;
  pivote_id: any = null;


  miFormulario: FormGroup = this.fb.group({
    creche_id: ['0', [Validators.nullValidator]],
  });

  miFormularioQuote: FormGroup = this.fb.group({
    request_id: [this.request_id, [Validators.required]],
    attended: [2, [Validators.nullValidator]],
    date: ['', [Validators.required]],
    hour: ['', [Validators.required]]
  });

  constructor(private elementRef: ElementRef, private fb: FormBuilder, private allService: AllService, private crecheService: CrecheService) {
    this.headers = [];
    this.data = [];

    this.searchTermChanged.pipe(
      debounceTime(1000) // Cambia este valor según el tiempo que desees esperar después de escribir
    ).subscribe(() => {
      // Realiza la acción deseada aquí, como realizar una búsqueda
      this.realizarBusqueda();
    });
  }

  searchTerm: any = '';
  searchTermChanged: Subject<string> = new Subject<string>();

  realizarBusqueda() {
    this.crecheService.searchValueQuote(this.searchTerm).subscribe({
      next: (res) => {
        this.data = res;
      }
    })
  }

  onSearchTermChange(searchTerm: string) {
    this.searchTermChanged.next(searchTerm);
  }

  search(value: any) {
    this.crecheService.searchQuote(value).subscribe({
      next: (res) => {
        this.data = res;
      }
    })
  }

  // Obtener la fecha actual en formato ISO (por ejemplo, "2023-09-15")
  currentDate = new Date().toISOString().split('T')[0];

  // Función para calcular la fecha mínima permitida (día siguiente a la fecha actual)
  minDate() {
    const tomorrow = new Date();
    tomorrow.setDate(new Date().getDate() + 1); // Obtener el día siguiente
    return tomorrow.toISOString().split('T')[0];
  }

  changeStatus(data: any) {
    Swal.fire({
      position: 'center',
      icon: 'question',
      title: '¿Está seguro de que desea registar la ' + (data.attended == 1 ? 'asistencia' : (data.attended == 0 ? 'falta' : '')) + ' para esta cita?',
      showConfirmButton: true,
      showCancelButton: true,
      confirmButtonText: 'Si',
      cancelButtonText: `No`
    }).then((result) => {
      if (result.isConfirmed) {
        this.allService.updateQuote(data).subscribe(response => {
          if (response.code == 200) {
            Swal.fire({
              position: 'center',
              icon: 'success',
              title: response.message,
              showConfirmButton: false,
              timer: 2000
            })
            this.asistenciaCreada.emit();
          } else {
            Swal.fire("Error", "error")
          }
        })
      } else if (result.isDenied) {
        return;
      }
    })
  }

  getCreches(data: any) {

    this.quote_id = data.id;
    this.request_id = data.request_id;
    this.pivote_id = data.pivote_id;

    this.crecheService.requestCreche(data).subscribe(response => {
      if (response) {

        this.creches = response;
      } else {

        Swal.fire("Error", "error")
      }
    })
    this.beneficiary = data.beneficiary;
  }

  addBeneficiaryService(data: any) {

    this.allService.addBeneficiaryService(data).subscribe({
      next: (response) => {
        if (response.code == 200) {

          Swal.fire({
            position: 'center',
            icon: 'success',
            title: response.message,
            showConfirmButton: false,
            timer: 2000
          })
          this.asistenciaCreada.emit();
        } else {

          Swal.fire({
            position: 'center',
            icon: 'error',
            title: response.message,
            showConfirmButton: false,
            timer: 2000
          })
        }
      }, error: (error) => {

        Swal.fire("Error", "error");
      }
    });
  }

  downloadFormat(id: any) {

    this.crecheService.downloadPdf(id).subscribe(response => {
      const blob: Blob = response;
      const url = window.URL.createObjectURL(blob);
      const a = document.createElement('a');
      a.href = url;
      a.download = `Solicitud_${id}.pdf`;
      a.click();
      window.URL.revokeObjectURL(url);
    });

  }

  downloadCiszFormat(id: any) {

    this.crecheService.downloadCiszPdf(id).subscribe(response => {
      const blob: Blob = response;
      const url = window.URL.createObjectURL(blob);
      const a = document.createElement('a');
      a.href = url;
      a.download = `Solicitud_${id}.pdf`;
      a.click();
      window.URL.revokeObjectURL(url);
    });

  }


  assignCreche() {
    const form = this.miFormulario.value;
    const [id, capacity, name] = form.creche_id.split(',');
    if (form == 0) {

      return;
    }
    const data = {}
    data['creche_id'] = id;
    data['quota'] = capacity;
    data['beneficiary_id'] = this.beneficiary.id;
    data['quote_id'] = this.quote_id;
    data['request_id'] = this.request_id;
    data['pivote_id'] = this.pivote_id;

    Swal.fire({
      position: 'center',
      icon: 'question',
      title: '¿Está seguro de que desea dar de alta a este usuario en la sala ' + name + ' ?',
      showConfirmButton: true,
      showCancelButton: true,
      confirmButtonText: 'Si',
      cancelButtonText: `No`
    }).then((result) => {
      if (result.isConfirmed) {
        this.crecheService.createBeneficiaryCreche(data).subscribe({
          next: (response) => {
            if (response.code == 200) {
              Swal.fire({
                position: 'center',
                icon: 'success',
                title: response.message,
                showConfirmButton: false,
                timer: 2000
              })
              this.asistenciaCreada.emit();
            } else {
              Swal.fire({
                position: 'center',
                icon: 'error',
                title: response.message,
                showConfirmButton: false,
                timer: 2000
              })
            }
          }, error: (error) => {
            Swal.fire("Error", "error")
          }
        })
      } else if (result.isDenied) {
        return;
      }
    })
  }

  reagendar() {
    this.miFormularioQuote.patchValue({
      request_id: this.request_id
    });
    if (this.miFormularioQuote.invalid) {
      return this.miFormularioQuote.markAllAsTouched();
    }
    Swal.fire({
      position: 'center',
      icon: 'question',
      title: '¿¿Está seguro de que desea reagendar cita el día ' + this.formatDate(this.miFormularioQuote.value.date) + ' en horario de ' + this.formatTime(this.miFormularioQuote.value.hour) + ' para esta solicitud??',
      showConfirmButton: true,
      showCancelButton: true,
      confirmButtonText: 'Si',
      cancelButtonText: `No`
    }).then((result) => {
      if (result.isConfirmed) {
        const data = this.miFormularioQuote.value;
        this.miFormulario.reset();
        this.allService.createQuote(data).subscribe({
          next: (response) => {
            if (response.code == 200) {
              Swal.fire({
                position: 'center',
                icon: 'success',
                title: response.message,
                showConfirmButton: false,
                timer: 2000
              })
              this.asistenciaCreada.emit();
              this.cerrarModal();
            }
          }, error: (error) => {
            Swal.fire("Error", "error")
          }
        })
      } else if (result.isDenied) {
        return;
      }
    })

  }

  getRequest(id: any, data: any) {
    this.request_id = id;
    this.beneficiary = data;
  }

  cerrarModal() {
    const botonCancel: any = this.elementRef.nativeElement.querySelector('#cancel');
    botonCancel.click();
  }

  // Método para formatear la fecha en formato día-mes-año
  formatDate(date: Date): string {
    const dateObject = new Date(date);
    const day = dateObject.getDate();
    const month = dateObject.getMonth() + 1;
    const year = dateObject.getFullYear();
    return `${day < 10 ? '0' + day : day}-${month < 10 ? '0' + month : month}-${year}`;
  }

  // Método para formatear la hora en formato 12 horas
  formatTime(date: string): string {
    const [hourStr, minutesStr] = date.split(':'); // Dividir la cadena en horas y minutos
    let hour = parseInt(hourStr);
    let zone = 'AM';
    // Determinar si es AM o PM
    if (hour >= 12) {
      zone = 'PM';
      hour = hour === 12 ? hour : hour - 12; // Convertir horas de 24 a 12 horas
    }
    // Ajustar la hora si es medianoche (hora 0)
    if (hour === 0) {
      hour = 12;
    }

    return `${hour}:${minutesStr} ${zone}`;
  }

}
