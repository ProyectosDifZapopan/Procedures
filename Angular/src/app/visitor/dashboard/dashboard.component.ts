import { Component, ElementRef, OnInit } from '@angular/core';
import { AllVisitorService } from '../services/all-visitor.service';
import { CrecheService } from '../services/creche.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  constructor(private crecheService: CrecheService, private allService: AllVisitorService, private elementRef: ElementRef) {
  }

  requests: any = [];
  beneficiaries: any = [];
  request: number;
  housing: any;
  documents: any;
  locations: any = null;
  selectLocation: any = "";
  selectCreche: any = "";
  creches: any = [];
  procedure_id: any;
  center_id: any;
  degree_id: any;
  requestAll: any;
  
  selectDependence: any = '';
  dependences: any = [];
  job_position: any = '';
  employee_number: any = '';

  services = [
    {
      title: 'Servicio 1',
      description: 'Descripción del servicio 1',
      image: 'https://ejemplo.com/imagen1.jpg'
    },
    {
      title: 'Servicio 2',
      description: 'Descripción del servicio 2',
      image: 'https://ejemplo.com/imagen2.jpg'
    },
    {
      title: 'Servicio 3',
      description: 'Descripción del servicio 3',
      image: 'https://ejemplo.com/imagen3.jpg'
    }
  ];

  ngOnInit(): void {
    this.crecheService.getCatalogs().subscribe({
      next: (catalogs) => {
        this.dependences = catalogs.dependencias;
      }
    });
    // Obtener las selecciones almacenadas en el servicio
    // let procedure = this.allService.getProcedure();
    // let center = this.allService.getCenter();
    // let room = this.allService.getRoom();
    let job_position = this.allService.getJob_position();
    let dependence_id = this.allService.getDependence_id();
    let employee_number = this.allService.getEmployee_number();

    if (job_position != null && dependence_id != null && employee_number != null) {
      // Limpiar datos después de un envío exitoso
      this.allService.clearJob_position();
      this.allService.clearDependence_id();
      this.allService.clearEmployee_number();
      let data = {
        'procedure_id': localStorage.getItem('procedure'),
        'center_id': 14,
        'job_position': job_position,
        'dependence_id': dependence_id,
        'employee_number': employee_number
      };
      
      this.crecheService.createRequest(data).subscribe({
        next: (response) => {
          if (response.code == 200) {
            Swal.fire({
              position: 'center',
              icon: 'success',
              title: response.message,
              showConfirmButton: true
            })
            this.loadCards();
          } else {
            Swal.fire({
              position: 'center',
              icon: 'error',
              title: response.message,
              showConfirmButton: false,
              timer: 2000
            })
          }
        }, error: (error) => {
          Swal.fire("Error", error);
        }
      })
    }

    this.loadCards();
  }

  cerrarModalSP() {
    const boton = this.elementRef.nativeElement.querySelector('#closeSPModal');
    boton.click();
  }

  loadCards() {
    this.allService.indexRequestVisitor().subscribe({
      next: (response) => {
        this.requests = response.data;
        console.log(this.requests);
      }
    })
  }

  setBeneficiaries(beneficiary: any, request: number, housing: any, requestAll: any) {
    this.beneficiaries = beneficiary;
    this.request = request;
    this.housing = housing;
    this.documents = requestAll.request_documents;
    this.requestAll = requestAll;
  }

  createRequest() {
    
    let data = {
      'procedure_id': localStorage.getItem('procedure'),
      'center_id': this.selectLocation,
      'degree_id': this.selectCreche ?? null,
      'job_position': this.job_position ?? null,
      'dependence_id': this.selectDependence ?? null,
      'employee_number': this.employee_number ?? null
    };
    this.crecheService.createRequest(data).subscribe({
      next: (response) => {
        if (response.code == 200) {
          Swal.fire({
            position: 'center',
            icon: 'success',
            title: response.message,
            showConfirmButton: true
          })
          this.allService.indexRequestVisitor().subscribe({
            next: (response) => {
              this.requests = response.data;
              console.log(this.requests);
            }
          })
        } else {
          Swal.fire({
            position: 'center',
            icon: 'error',
            title: response.message,
            showConfirmButton: false,
            timer: 2000
          })
        }
      }, error: (error) => {
        Swal.fire("Error", error);
      }
    });
    
  }

  changeStatusRequest(request_id: any, status_id: any){
    
    this.allService.changeStatusRequest({ id: request_id, status_request_id: status_id }).subscribe({
      next: (response) => {
        
        if (response.code == 200) {
          Swal.fire({
            position: 'center',
            icon: 'success',
            title: response.message,
            showConfirmButton: true
          })
          
          this.loadCards();
        } else {
          Swal.fire({
            position: 'center',
            icon: 'error',
            title: response.message,
            showConfirmButton: false,
            timer: 2000
          })
          
        }
      }, error: (error) => {
        Swal.fire("Error", error);
      }
    })
    
  }

  finishRequest(request_id: any, status_id: any) {
    if(status_id== 6){
      Swal.fire({
        position: 'center',
        icon: 'info',
        title: 'Esta acción cancelará la solicitud. ¿desea continuar?',
        showConfirmButton: true,
        showCancelButton: true,
        confirmButtonText: 'Continuar',
        cancelButtonText: `No`
      }).then((result) => {
        if (result.isConfirmed) {
          this.changeStatusRequest(request_id,status_id);
        } else if (result.isDenied) {
          return
        }
      })
    }else{
      this.changeStatusRequest(request_id,status_id);
    }
  }

  setProcedure(id: any) {
    localStorage.removeItem('procedure');
    localStorage.setItem('procedure', id);

    if (id == 1) {
      this.createRequestCisz();
    }
  }

  createRequestCisz() {
    this.selectLocation = 14;
    if (this.employee_number == '' || this.job_position == '' || this.selectDependence == 's') {
      Swal.fire({
        position: 'center',
        icon: 'warning',
        title: 'Ingrese dependencia, número de empleado y puesto.',
        showConfirmButton: false,
        timer: 2000
      })
    } else{
      Swal.fire({
        position: 'center',
        icon: 'info',
        title: 'Esta información será revisada para verificar su veracidad. ¿desea continuar?',
        showConfirmButton: true,
        showCancelButton: true,
        confirmButtonText: 'Continuar',
        cancelButtonText: `No`
      }).then((result) => {
        if (result.isConfirmed) {
          this.createRequest();
          this.cerrarModalSP();
        } else if (result.isDenied) {
          localStorage.removeItem('procedure');
        }
      })
    }
  }


  getCreches() {
    
    this.crecheService.getCreches(this.selectLocation).subscribe({
      next: (response) => {
        this.creches = response;
      }
    })
    
  }

  getLocations(id: number) {
    
    this.crecheService.getLocations(id).subscribe({
      next: (response) => {
        this.locations = response;
      }
    })
    
  }
}