import { Component, ElementRef } from '@angular/core';
import { CrecheService } from '../services/creche.service';
import { Router } from '@angular/router';
import { AllVisitorService } from '../services/all-visitor.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-homepage',
  templateUrl: './homepage.component.html',
  styleUrls: ['./homepage.component.css']
})
export class HomepageComponent {

  constructor(private router: Router, private crecheService: CrecheService, private allService: AllVisitorService,private elementRef: ElementRef) { }
  locations: any = null;
  selectLocation: any = "";
  selectCreche: any = "";
  creches: any = [];
  
  selectDependence: any = '';
  dependences: any = [];
  job_position: any = '';
  employee_number: any = '';

  services = [
    {
      title: 'Servicio 1',
      description: 'Descripción del servicio 1',
      image: 'https://ejemplo.com/imagen1.jpg'
    },
    {
      title: 'Servicio 2',
      description: 'Descripción del servicio 2',
      image: 'https://ejemplo.com/imagen2.jpg'
    },
    {
      title: 'Servicio 3',
      description: 'Descripción del servicio 3',
      image: 'https://ejemplo.com/imagen3.jpg'
    }
  ];

  getLocations(id: number) {
    this.crecheService.getLocations(id).subscribe({
      next: (response) => {
        this.locations = response;
      }
    })
  }

  cerrarModalSP() {
    const boton = this.elementRef.nativeElement.querySelector('#closeSPModal');
    boton.click();
  }


  ngOnInit(): void {
    this.crecheService.getCatalogs().subscribe({
      next: (catalogs) => {
        this.dependences = catalogs.dependencias;
      }
    });
  }

  createRequestFin(){
    let data = {
      'procedure_id' :localStorage.getItem('procedure'),
      'center_id' : 14,
      'job_position': this.job_position ?? null,
      'dependence_id': this.selectDependence ?? null,
      'employee_number': this.employee_number ?? null
    };
    this.crecheService.createRequest(data).subscribe({
      next: (response) => {
        
        this.cerrarModalSP();
        if (response.code == 200) {
          
          this.router.navigateByUrl('/dashboard');
        }
        if (response.code == 404) {
          
        this.allService.setSelections(localStorage.getItem('procedure'), this.job_position, this.selectDependence, this.employee_number);
          this.router.navigateByUrl('/auth/login');
        }
      }, error: (error) => {
        
        console.log(error);
      }
    })
  }

  createRequest() {
    if (this.employee_number == '' || this.job_position == '' || this.selectDependence == 's') {
      Swal.fire({
        position: 'center',
        icon: 'warning',
        title: 'Ingresa dependencia, numero de empleado y puesto.',
        showConfirmButton: false,
        timer: 2000
      })
    } else{
      Swal.fire({
        position: 'center',
        icon: 'info',
        title: 'Esta información será revisada para verificar su veracidad ¿desea continuar?',
        showConfirmButton: true,
        showCancelButton: true,
        confirmButtonText: 'Continuar',
        cancelButtonText: `No`
      }).then((result) => {
        if (result.isConfirmed) {
          this.createRequestFin();
        } else if (result.isDenied) {
          return
        }
      })
    }
  }

  setProcedure(id: any) {
    localStorage.removeItem('procedure');
    localStorage.setItem('procedure', id);
  }

  getCreches() {
    this.crecheService.getCreches(this.selectLocation).subscribe({
      next: (response) => {
        this.creches = response;
      }
    })
  }
}
