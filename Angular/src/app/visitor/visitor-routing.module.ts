import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'guarderia',
    loadChildren: () => import('./creche/creche.module').then(m => m.CrecheModule)
  },
  {
    path: 'cisz-guarderia',
    loadChildren: () => import('./cisz-creche/cisz-creche.module').then(m => m.CiszCrecheModule)
  },
  {
    path: 'autismo',
    loadChildren: () => import('./autism/autism.module').then(m => m.AutismModule)
  },
  {
    path: 'dashboard',
    loadChildren: () => import('./dashboard/dashboard.module').then(m => m.DashboardModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class VisitorRoutingModule { }
