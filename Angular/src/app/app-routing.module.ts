import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ValidarRolTramiteGuard } from './guards/validar-rol-tramite.guard';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  {
    path: 'auth',
    loadChildren: () => import('./auth/auth.module').then(m => m.AuthModule)
  },
  {
    path: 'admin',
    loadChildren: () => import('./protected/protected.module').then(m => m.ProtectedModule),
    canActivateChild: [ValidarRolTramiteGuard],
    data: { expectedRoles: [2], expectedPlatforms: [1] }
  },
  {
    path: '',
    loadChildren: () => import('./visitor/visitor.module').then(m => m.VisitorModule),
    canActivateChild: [ValidarRolTramiteGuard],
    data: { expectedRoles: [1] }
  },
  {
    path: 'home',
    loadChildren: () => import('./visitor/homepage/homepage.module').then(m => m.HomepageModule),
  },
  {
    path: '**',
    redirectTo: 'home'
  },
];


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
