export const environment = {
  production: true,
  baseUrl:"https://tramites.difzapopan.gob.mx/laravel_jwt/public/api",
  dowload: "https://tramites.difzapopan.gob.mx/laravel_jwt/public/documents/"
};
