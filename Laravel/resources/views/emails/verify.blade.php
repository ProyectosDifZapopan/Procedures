<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Verificación de Cuenta</title>
</head>
<body style="font-family: Arial, sans-serif;">

    <h1>Verificación de Cuenta</h1>

    <p>Hola {{ $name }},</p>

    <p>Recibiste este correo para la verificación de tu cuenta.</p>

    <p>Para verificar tu cuenta, ingresa el codigo en la pantalla de VERIFICACIÓN:</p>

    <p><strong>{{ $number }}</strong></p>

    <p>Si no solicitaste la verificación, puedes ignorar este correo.</p>

    <p>¡Gracias!</p>

</body>
</html>