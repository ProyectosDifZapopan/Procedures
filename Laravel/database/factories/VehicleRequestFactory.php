<?php

namespace Database\Factories;

use App\Models\Requests;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\VehicleRequest>
 */
class VehicleRequestFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'marca'=>$this->faker->firstName(),
            'tipo'=>$this->faker->firstName(),
            'modelo'=>$this->faker->firstName(),
            'valor_aproximado'=>$this->faker->numberBetween(10000,200000),
            'request_id'=>Requests::all()->random()->id,
        ];
    }
}
