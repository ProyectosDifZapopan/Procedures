<?php

namespace Database\Factories;

use App\Models\Requests;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\HousingRequest>
 */
class HousingRequestFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $horarios = ['08:00', '10:00', '14:00', '16:00'];
        return [
            'propia' => $this->faker->randomElement([1, 0]),
            'valor_estimado' => $this->faker->numberBetween(100000, 2000000),
            'tipo_vivienda' => $this->faker->numberBetween(1, 5),
            'pago_mensual' => $this->faker->numberBetween(5000, 10000),
            'arrendador_propietario' => $this->faker->firstName(),
            'calle' => $this->faker->streetName(),
            'numext' => $this->faker->numberBetween(1, 1000),
            'numint' => $this->faker->numberBetween(1, 5),
            'primercruce' => $this->faker->streetName(),
            'segundocruce' => $this->faker->streetName(),
            'codigo' => 44700,
            'colonia' => 57802,
            'municipio' => $this->faker->city(),
            'telefono' => 3331974977,
            'contrato' => $this->faker->randomElement([1, 0]),
            'vive_con_familiares' => $this->faker->randomElement([1, 0]),
            'vencimiento_contrato' => $this->faker->dateTimeBetween(now()),
            'cantidad_aporta' => $this->faker->numberBetween(500, 1000),
            'horario_en_casa' => $this->faker->randomElement($horarios),
            'request_id' => Requests::all()->random()->id,
        ];
    }
}
