<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;

use App\Models\Center;
use App\Models\CenterProcedure;
use App\Models\Creche;
use App\Models\Degree;
use App\Models\Department;
use App\Models\DocumentType;
use App\Models\Form;
use App\Models\MessageMotive;
use App\Models\Priority;
use App\Models\Procedure;
use App\Models\RequiredDocument;
use App\Models\Role;
use App\Models\Room;
use App\Models\StatusRequest;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        Priority::factory()->create([
            'name'                  => 'BAJA'
        ]);
        Priority::factory()->create([
            'name'                  => 'MEDIA'
        ]);
        Priority::factory()->create([
            'name'                  => 'ALTA'
        ]);
        StatusRequest::create([
            'name'  => 'SIN TERMINAR'
        ]);
        StatusRequest::create([
            'name'  => 'EN REVISIÓN'
        ]);
        StatusRequest::create([
            'name'  => 'ACEPTADO'
        ]);
        StatusRequest::create([
            'name'  => 'RECHAZADO'
        ]);
        StatusRequest::create([
            'name'  => 'EN ESPERA'
        ]);
        StatusRequest::create([
            'name'  => 'CANCELADA'
        ]);
        StatusRequest::create([
            'name'  => 'COMPLETADA'
        ]);
        StatusRequest::create([
            'name'  => 'MODIFICANDO'
        ]);
        // Priority::factory(3)->create();
        // Center::factory(10)->create();
        Center::create([
            'name'  => 'NIDO 1-CARMEN ARCE ZUNO',
            'address' => 'PENDIENTE',
            'latitude' => 25.545455445,
            'longitude' => 1.24158424121
        ]);
        Center::create([
            'name'  => 'NIDO 2-PABLO CASALS',
            'address' => 'PENDIENTE',
            'latitude' => 25.545455445,
            'longitude' => 1.24158424121
        ]);
        Center::create([
            'name'  => 'NIDO 3-DRA. IRENE ROBLEDO GARCIA',
            'address' => 'PENDIENTE',
            'latitude' => 25.545455445,
            'longitude' => 1.24158424121
        ]);
        Center::create([
            'name'  => 'NIDO 4-MELVIN JONES',
            'address' => 'PENDIENTE',
            'latitude' => 25.545455445,
            'longitude' => 1.24158424121
        ]);
        Center::create([
            'name'  => 'NIDO 5-EL COLLI',
            'address' => 'PENDIENTE',
            'latitude' => 25.545455445,
            'longitude' => 1.24158424121
        ]);
        Center::create([
            'name'  => 'NIDO 6-TABACHINES',
            'address' => 'PENDIENTE',
            'latitude' => 25.545455445,
            'longitude' => 1.24158424121
        ]);
        Center::create([
            'name'  => 'NIDO 8-MARIA JAIME FRANCO',
            'address' => 'PENDIENTE',
            'latitude' => 25.545455445,
            'longitude' => 1.24158424121
        ]);
        Center::create([
            'name'  => 'NIDO 9-VILLAS DE GUADALUPE',
            'address' => 'PENDIENTE',
            'latitude' => 25.545455445,
            'longitude' => 1.24158424121
        ]);
        Center::create([
            'name'  => 'NIDO 10-DEL MAR',
            'address' => 'PENDIENTE',
            'latitude' => 25.545455445,
            'longitude' => 1.24158424121
        ]);
        Center::create([
            'name'  => 'NIDO COMUNITARIO COTOS',
            'address' => 'PENDIENTE',
            'latitude' => 25.545455445,
            'longitude' => 1.24158424121
        ]);
        Center::create([
            'name'  => 'NIDO COMUNITARIO MIRAMAR',
            'address' => 'PENDIENTE',
            'latitude' => 25.545455445,
            'longitude' => 1.24158424121
        ]);
        Center::create([
            'name'  => 'NIDO COMUNITARIO HIGUERA',
            'address' => 'PENDIENTE',
            'latitude' => 25.545455445,
            'longitude' => 1.24158424121
        ]);
        Center::create([
            'name'  => 'NIDO COMUNITARIO CORONILLA',
            'address' => 'PENDIENTE',
            'latitude' => 25.545455445,
            'longitude' => 1.24158424121
        ]);
        Center::create([
            'name'  => 'NIDO CISZ',
            'address' => 'PENDIENTE',
            'latitude' => 25.545455445,
            'longitude' => 1.24158424121
        ]);
        Department::create([
            'name'  => 'CISZ',
        ]);
        Department::create([
            'name'  => 'CDI',
        ]);
        Department::create([
            'name'  => 'AUTISMO',
        ]);
        Department::create([
            'name'  => 'HABILITECAS',
        ]);
        // Department::factory(5)->create();
        // Role::factory(3)->create();
        Role::create([
            'name'  => 'VISITANTE',
        ]);
        Role::create([
            'name'  => 'ADMIN CENTRO',
        ]);
        Role::create([
            'name'  => 'ADMIN DEPARTAMENTO',
        ]);
        Role::create([
            'name'  => 'ADMIN GENERAL',
        ]);
        // User::factory(200)->create();
        // DocumentType::factory(5)->create();
        // Visitor::factory(200)->create();
        // Questions::factory(10)->create();
        // Procedure::factory(4)->create();
        Procedure::create([
            'name' => 'Nido cisz'
        ]);
        Procedure::create([
            'name' => 'Guarderías'
        ]);
        Procedure::create([
            'name' => 'Habilitecas'
        ]);
        Procedure::create([
            'name' => 'CAP'
        ]);
        Procedure::create([
            'name' => 'CEMAM'
        ]);
        Procedure::create([
            'name' => 'Autismo'
        ]);
        Form::create([
            'name' => 'INFANTE',
            'procedure_id' => 2,
            'description' => '',
            'url' => '/beneficiario'
        ]);
        Form::create([
            'name' => 'PADRES/TUTORES',
            'procedure_id' => 2,
            'description' => '',
            'url' => '/padres'
        ]);
        Form::create([
            'name' => 'DOCUMENTACIÓN',
            'procedure_id' => 2,
            'description' => '',
            'url' => '/documentos'
        ]);
        Form::create([
            'name' => 'VIVIENDA',
            'procedure_id' => 2,
            'description' => '',
            'url' => '/vivienda'
        ]);
        Form::create([
            'name' => 'VEHICULOS',
            'procedure_id' => 2,
            'description' => '',
            'url' => '/vivienda'
        ]);
        Form::create([
            'name' => 'REFERENCIAS',
            'procedure_id' => 2,
            'description' => '',
            'url' => '/referencia'
        ]);
        // QuestionProcedure::factory(4)->create();
        // Requests::factory(120)->create();
        // RequiredDocument::factory(10)->create();
        // Answer::factory(150)->create();
        // Beneficiary::factory(300)->create();
        // RequestDocument::factory(120)->create();
        // BeneficiaryRequest::factory(150)->create();
        // Quote::factory(60)->create();
        // Degree::factory(5)->create();
        // Address::factory(200)->create();
        // AddressBeneficiary::factory(300)->create();
        Degree::factory()->create([
            'name'  => 'LACTANTE'
        ]);
        Degree::factory()->create([
            'name'  => 'MATERNAL'
        ]);
        Degree::factory()->create([
            'name'  => 'PRESCOLAR'
        ]);
        // Room::factory(5)->create();
        Room::factory()->create([
            'name'  => 'LAC'
        ]);
        Room::factory()->create([
            'name'  => 'MAT A'
        ]);
        Room::factory()->create([
            'name'  => 'MAT AB'
        ]);
        Room::factory()->create([
            'name'  => 'MAT BC'
        ]);
        Room::factory()->create([
            'name'  => 'MAT C'
        ]);
        Room::factory()->create([
            'name'  => 'PRES 1'
        ]);
        Room::factory()->create([
            'name'  => 'PRES 1 A'
        ]);
        Room::factory()->create([
            'name'  => 'PRES 2'
        ]);
        Room::factory()->create([
            'name'  => 'PRES 3'
        ]);
        Room::factory()->create([
            'name'  => 'LAC A'
        ]);
        Room::factory()->create([
            'name'  => 'LAC B'
        ]);
        Room::factory()->create([
            'name'  => 'LAC C'
        ]);
        Room::factory()->create([
            'name'  => 'MAT B'
        ]);
        // CenterProcedure::factory(10)->create();
        CenterProcedure::factory()->create([
            'procedure_id'  => 2,
            'center_id' => 1
        ]);
        CenterProcedure::factory()->create([
            'procedure_id'  => 2,
            'center_id' => 2
        ]);
        CenterProcedure::factory()->create([
            'procedure_id'  => 2,
            'center_id' => 3
        ]);
        CenterProcedure::factory()->create([
            'procedure_id'  => 2,
            'center_id' => 4
        ]);
        CenterProcedure::factory()->create([
            'procedure_id'  => 2,
            'center_id' => 5
        ]);
        CenterProcedure::factory()->create([
            'procedure_id'  => 2,
            'center_id' => 6
        ]);
        CenterProcedure::factory()->create([
            'procedure_id'  => 2,
            'center_id' => 7
        ]);
        CenterProcedure::factory()->create([
            'procedure_id'  => 2,
            'center_id' => 8
        ]);
        CenterProcedure::factory()->create([
            'procedure_id'  => 2,
            'center_id' => 9
        ]);
        CenterProcedure::factory()->create([
            'procedure_id'  => 2,
            'center_id' => 10
        ]);
        CenterProcedure::factory()->create([
            'procedure_id'  => 2,
            'center_id' => 11
        ]);
        CenterProcedure::factory()->create([
            'procedure_id'  => 2,
            'center_id' => 12
        ]);
        CenterProcedure::factory()->create([
            'procedure_id'  => 2,
            'center_id' => 13
        ]);
        CenterProcedure::factory()->create([
            'procedure_id'  => 1,
            'center_id' => 14
        ]);
        // Creche::factory(15)->create();
        Creche::factory()->create([
            'capacity'  => 12,
            'degree_id' => 1,
            'room_id' => 1,
            'center_id' => 1
        ]);
        Creche::factory()->create([
            'capacity'  => 14,
            'degree_id' => 2,
            'room_id' => 3,
            'center_id' => 1
        ]);
        Creche::factory()->create([
            'capacity'  => 14,
            'degree_id' => 2,
            'room_id' => 5,
            'center_id' => 1
        ]);
        Creche::factory()->create([
            'capacity'  => 20,
            'degree_id' => 3,
            'room_id' => 7,
            'center_id' => 1
        ]);
        Creche::factory()->create([
            'capacity'  => 20,
            'degree_id' => 3,
            'room_id' => 8,
            'center_id' => 1
        ]);
        Creche::factory()->create([
            'capacity'  => 20,
            'degree_id' => 3,
            'room_id' => 9,
            'center_id' => 1
        ]);
        Creche::factory()->create([
            'capacity'  => 10,
            'degree_id' => 1,
            'room_id' => 1,
            'center_id' => 2
        ]);
        Creche::factory()->create([
            'capacity'  => 10,
            'degree_id' => 2,
            'room_id' => 3,
            'center_id' => 2
        ]);
        Creche::factory()->create([
            'capacity'  => 11,
            'degree_id' => 2,
            'room_id' => 5,
            'center_id' => 2
        ]);
        Creche::factory()->create([
            'capacity'  => 23,
            'degree_id' => 3,
            'room_id' => 7,
            'center_id' => 2
        ]);
        Creche::factory()->create([
            'capacity'  => 23,
            'degree_id' => 3,
            'room_id' => 8,
            'center_id' => 2
        ]);
        Creche::factory()->create([
            'capacity'  => 23,
            'degree_id' => 3,
            'room_id' => 9,
            'center_id' => 2
        ]);
        Creche::factory()->create([
            'capacity'  => 12,
            'degree_id' => 2,
            'room_id' => 2,
            'center_id' => 3
        ]);
        Creche::factory()->create([
            'capacity'  => 16,
            'degree_id' => 2,
            'room_id' => 4,
            'center_id' => 3
        ]);
        Creche::factory()->create([
            'capacity'  => 22,
            'degree_id' => 3,
            'room_id' => 7,
            'center_id' => 3
        ]);
        Creche::factory()->create([
            'capacity'  => 25,
            'degree_id' => 3,
            'room_id' => 8,
            'center_id' => 3
        ]);
        Creche::factory()->create([
            'capacity'  => 25,
            'degree_id' => 3,
            'room_id' => 9,
            'center_id' => 3
        ]);
        Creche::factory()->create([
            'capacity'  => 4,
            'degree_id' => 1,
            'room_id' => 1,
            'center_id' => 4
        ]);
        Creche::factory()->create([
            'capacity'  => 7,
            'degree_id' => 2,
            'room_id' => 3,
            'center_id' => 4
        ]);
        Creche::factory()->create([
            'capacity'  => 14,
            'degree_id' => 2,
            'room_id' => 5,
            'center_id' => 4
        ]);
        Creche::factory()->create([
            'capacity'  => 22,
            'degree_id' => 3,
            'room_id' => 7,
            'center_id' => 4
        ]);
        Creche::factory()->create([
            'capacity'  => 24,
            'degree_id' => 3,
            'room_id' => 8,
            'center_id' => 4
        ]);
        Creche::factory()->create([
            'capacity'  => 24,
            'degree_id' => 3,
            'room_id' => 9,
            'center_id' => 4
        ]);
        Creche::factory()->create([
            'capacity'  => 8,
            'degree_id' => 1,
            'room_id' => 1,
            'center_id' => 5
        ]);
        Creche::factory()->create([
            'capacity'  => 14,
            'degree_id' => 2,
            'room_id' => 2,
            'center_id' => 5
        ]);
        Creche::factory()->create([
            'capacity'  => 14,
            'degree_id' => 2,
            'room_id' => 4,
            'center_id' => 5
        ]);
        Creche::factory()->create([
            'capacity'  => 20,
            'degree_id' => 3,
            'room_id' => 6,
            'center_id' => 5
        ]);
        Creche::factory()->create([
            'capacity'  => 20,
            'degree_id' => 3,
            'room_id' => 8,
            'center_id' => 5
        ]);
        Creche::factory()->create([
            'capacity'  => 20,
            'degree_id' => 3,
            'room_id' => 9,
            'center_id' => 5
        ]);
        Creche::factory()->create([
            'capacity'  => 10,
            'degree_id' => 1,
            'room_id' => 1,
            'center_id' => 6
        ]);
        Creche::factory()->create([
            'capacity'  => 8,
            'degree_id' => 2,
            'room_id' => 2,
            'center_id' => 6
        ]);
        Creche::factory()->create([
            'capacity'  => 14,
            'degree_id' => 2,
            'room_id' => 4,
            'center_id' => 6
        ]);
        Creche::factory()->create([
            'capacity'  => 18,
            'degree_id' => 3,
            'room_id' => 6,
            'center_id' => 6
        ]);
        Creche::factory()->create([
            'capacity'  => 20,
            'degree_id' => 3,
            'room_id' => 8,
            'center_id' => 6
        ]);
        Creche::factory()->create([
            'capacity'  => 20,
            'degree_id' => 3,
            'room_id' => 9,
            'center_id' => 6
        ]);
        Creche::factory()->create([
            'capacity'  => 5,
            'degree_id' => 1,
            'room_id' => 1,
            'center_id' => 7
        ]);
        Creche::factory()->create([
            'capacity'  => 12,
            'degree_id' => 2,
            'room_id' => 3,
            'center_id' => 7
        ]);
        Creche::factory()->create([
            'capacity'  => 15,
            'degree_id' => 2,
            'room_id' => 5,
            'center_id' => 7
        ]);
        Creche::factory()->create([
            'capacity'  => 20,
            'degree_id' => 3,
            'room_id' => 7,
            'center_id' => 7
        ]);
        Creche::factory()->create([
            'capacity'  => 20,
            'degree_id' => 3,
            'room_id' => 8,
            'center_id' => 7
        ]);
        Creche::factory()->create([
            'capacity'  => 20,
            'degree_id' => 3,
            'room_id' => 9,
            'center_id' => 7
        ]);
        Creche::factory()->create([
            'capacity'  => 10,
            'degree_id' => 1,
            'room_id' => 1,
            'center_id' => 8
        ]);
        Creche::factory()->create([
            'capacity'  => 14,
            'degree_id' => 2,
            'room_id' => 3,
            'center_id' => 8
        ]);
        Creche::factory()->create([
            'capacity'  => 14,
            'degree_id' => 2,
            'room_id' => 5,
            'center_id' => 8
        ]);
        Creche::factory()->create([
            'capacity'  => 20,
            'degree_id' => 3,
            'room_id' => 7,
            'center_id' => 8
        ]);
        Creche::factory()->create([
            'capacity'  => 20,
            'degree_id' => 3,
            'room_id' => 8,
            'center_id' => 8
        ]);
        Creche::factory()->create([
            'capacity'  => 20,
            'degree_id' => 3,
            'room_id' => 9,
            'center_id' => 8
        ]);
        Creche::factory()->create([
            'capacity'  => 10,
            'degree_id' => 1,
            'room_id' => 1,
            'center_id' => 9
        ]);
        Creche::factory()->create([
            'capacity'  => 15,
            'degree_id' => 2,
            'room_id' => 2,
            'center_id' => 9
        ]);
        Creche::factory()->create([
            'capacity'  => 10,
            'degree_id' => 2,
            'room_id' => 3,
            'center_id' => 9
        ]);
        Creche::factory()->create([
            'capacity'  => 15,
            'degree_id' => 2,
            'room_id' => 5,
            'center_id' => 9
        ]);
        Creche::factory()->create([
            'capacity'  => 22,
            'degree_id' => 3,
            'room_id' => 6,
            'center_id' => 9
        ]);
        Creche::factory()->create([
            'capacity'  => 22,
            'degree_id' => 3,
            'room_id' => 8,
            'center_id' => 9
        ]);
        Creche::factory()->create([
            'capacity'  => 22,
            'degree_id' => 3,
            'room_id' => 9,
            'center_id' => 9
        ]);
        Creche::factory()->create([
            'capacity'  => 15,
            'degree_id' => 3,
            'room_id' => 7,
            'center_id' => 10
        ]);
        Creche::factory()->create([
            'capacity'  => 15,
            'degree_id' => 3,
            'room_id' => 8,
            'center_id' => 10
        ]);
        Creche::factory()->create([
            'capacity'  => 15,
            'degree_id' => 3,
            'room_id' => 9,
            'center_id' => 10
        ]);
        Creche::factory()->create([
            'capacity'  => 15,
            'degree_id' => 3,
            'room_id' => 7,
            'center_id' => 11
        ]);
        Creche::factory()->create([
            'capacity'  => 15,
            'degree_id' => 3,
            'room_id' => 8,
            'center_id' => 11
        ]);
        Creche::factory()->create([
            'capacity'  => 15,
            'degree_id' => 3,
            'room_id' => 9,
            'center_id' => 11
        ]);
        Creche::factory()->create([
            'capacity'  => 15,
            'degree_id' => 3,
            'room_id' => 7,
            'center_id' => 12
        ]);
        Creche::factory()->create([
            'capacity'  => 15,
            'degree_id' => 3,
            'room_id' => 8,
            'center_id' => 12
        ]);
        Creche::factory()->create([
            'capacity'  => 15,
            'degree_id' => 3,
            'room_id' => 9,
            'center_id' => 12
        ]);
        Creche::factory()->create([
            'capacity'  => 15,
            'degree_id' => 3,
            'room_id' => 7,
            'center_id' => 13
        ]);
        Creche::factory()->create([
            'capacity'  => 15,
            'degree_id' => 3,
            'room_id' => 8,
            'center_id' => 13
        ]);
        Creche::factory()->create([
            'capacity'  => 15,
            'degree_id' => 3,
            'room_id' => 9,
            'center_id' => 13
        ]);
//-----------------------------------------
        Creche::factory()->create([
            'capacity'  => 12,
            'degree_id' => 1,
            'room_id' => 10,
            'center_id' => 14
        ]);
        Creche::factory()->create([
            'capacity'  => 24,
            'degree_id' => 1,
            'room_id' => 11,
            'center_id' => 14
        ]);
        Creche::factory()->create([
            'capacity'  => 24,
            'degree_id' => 1,
            'room_id' => 12,
            'center_id' => 14
        ]);
        Creche::factory()->create([
            'capacity'  => 24,
            'degree_id' => 2,
            'room_id' => 2,
            'center_id' => 14
        ]);
        Creche::factory()->create([
            'capacity'  => 24,
            'degree_id' => 2,
            'room_id' => 13,
            'center_id' => 14
        ]);
        Creche::factory()->create([
            'capacity'  => 24,
            'degree_id' => 2,
            'room_id' => 5,
            'center_id' => 14
        ]);
        Creche::factory()->create([
            'capacity'  => 24,
            'degree_id' => 3,
            'room_id' => 6,
            'center_id' => 14
        ]);
        Creche::factory()->create([
            'capacity'  => 24,
            'degree_id' => 3,
            'room_id' => 8,
            'center_id' => 14
        ]);
        Creche::factory()->create([
            'capacity'  => 24,
            'degree_id' => 3,
            'room_id' => 9,
            'center_id' => 14
        ]);

        MessageMotive::create([
            'name' => 'OBSERVACIÓN'
        ]);
        MessageMotive::create([
            'name' => 'COMENTARIO'
        ]);
        MessageMotive::create([
            'name' => 'AVISO'
        ]);
        MessageMotive::create([
            'name' => 'CORRECCIÓN'
        ]);

        DocumentType::create([
            'name' => 'INE',
            'descripcion' => 'IMAGEN DE INE FRONTAL Y TRASERA.'
        ]);
        DocumentType::create([
            'name' => 'DOCUMENTO CISZ',
            'descripcion' => 'DOCUMENTO LLENADO.'
        ]);
        DocumentType::create([
            'name' => 'IDENTIFICACIÓN DE EMPLEADO',
            'descripcion' => 'IDENTIFICACIÓN DE EMPLEADO O INE FRONTRAL.'
        ]);

        RequiredDocument::create([
            'procedure_id' => 1,
            'document_type_id' => 2,
            'forced' => 1,
        ]);
        RequiredDocument::create([
            'procedure_id' => 1,
            'document_type_id' => 3,
            'forced' => 1,
        ]);

        RequiredDocument::create([
            'procedure_id' => 2,
            'document_type_id' => 1,
            'forced' => 1,
        ]);
        // CrecheRequest::factory(50)->create();
        // BeneficiaryCreche::factory(30)->create();
        // VehicleRequest::factory(50)->create();
        // EconomicBeneficiary::factory(70)->create();
        // HousingRequest::factory(50)->create();
        // ReferencesRequest::factory(150)->create();
    }
}
