<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('housing_requests', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('valor_estimado')->nullable();

            $table->string('tipo_vivienda');
            $table->string('pago_mensual')->nullable();
            $table->string('arrendador_propietario')->nullable();

            $table->string('telefono');
            $table->boolean('contrato')->nullable();
            $table->boolean('vive_con_familiares');
            $table->date('vencimiento_contrato')->nullable();

            $table->integer('cantidad_aporta')->nullable();;
            $table->time('horario_en_casa_1');
            $table->time('horario_en_casa_2');

            $table->unsignedBigInteger('request_id');
            $table->foreign('request_id')->references('id')->on('requests');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('housing_requests');
    }
};
