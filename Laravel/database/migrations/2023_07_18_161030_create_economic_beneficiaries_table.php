<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('economic_beneficiaries', function (Blueprint $table) {
            $table->id();
            $table->integer('ocupacion')->nullable();
            $table->string('parentesco');

            $table->string('lugar_nacimiento');
            $table->integer('estado_civil')->default(0);
            $table->integer('estado_religioso')->nullable();

            $table->boolean('trabaja')->default(1);

            //
            $table->string('puesto')->nullable();
            $table->string('antiguedad')->nullable();

            $table->string('lugar_trabajo')->nullable();

            //
            $table->string('calle_trabajo')->nullable();
            $table->integer('numext_trabajo')->nullable();
            $table->string('numint_trabajo', 3)->nullable();
            $table->string('primer_cruce_trabajo')->nullable();
            $table->string('segundo_cruce_trabajo')->nullable();
            $table->string('otro_trabajo')->nullable();

            $table->string('telefono_trabajo')->nullable();
            $table->string('jefe_inmediato')->nullable();
            $table->string('codigo_trabajo')->nullable();
            $table->string('colonia_trabajo')->nullable();
            $table->string('colonia_name_trabajo')->nullable();
            $table->string('municipio_trabajo')->nullable();
            $table->string('estado_trabajo')->nullable();
            $table->time('entrada_trabajo')->nullable();
            $table->time('salida_trabajo')->nullable();

            $table->string('ingreso_mensual_bruto')->nullable();
            $table->string('ingreso_mensual_neto')->nullable();

            $table->unsignedBigInteger('beneficiary_id');
            $table->foreign('beneficiary_id')->references('id')->on('beneficiaries');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('economic_beneficiaries');
    }
};
