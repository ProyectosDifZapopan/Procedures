<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('extra_works', function (Blueprint $table) {
            $table->id();

            $table->string('lugar')->nullable();
            $table->string('telefono')->nullable();
            $table->string('jefe')->nullable();
            $table->time('entrada')->nullable();
            $table->time('salida')->nullable();

            $table->unsignedBigInteger('beneficiary_id');
            $table->foreign('beneficiary_id')->references('id')->on('beneficiaries');
            
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('extra_works');
    }
};
