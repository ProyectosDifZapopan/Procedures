<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('references_requests', function (Blueprint $table) {
            $table->id();

            $table->string('nombre');
            $table->string('apaterno');
            $table->string('amaterno');
            // $table->string('domicilio');
            // $table->string('codigo');
            // $table->string('municipio');
            // $table->string('colonia');
            $table->text('calle');
            $table->string('numext', 25);
            $table->string('numint', 25)->nullable();
            $table->string('municipio');
            $table->string('estado');
            $table->integer('codigopostal');
            $table->text('colonia');
            $table->text('colonia_name');
            $table->string('telefono');
            $table->string('celular');

            $table->time('horario_contacto_1')->nullable();
            $table->time('horario_contacto_2')->nullable();

            $table->unsignedBigInteger('request_id');
            $table->foreign('request_id')->references('id')->on('requests');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('references_requests');
    }
};
