<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('beneficiaries', function (Blueprint $table) {
            $table->id();
            $table->string('curp',18);
            $table->string('nombre');
            $table->string('apaterno');
            $table->string('amaterno');
            $table->integer('escolaridad')->default(1);
            $table->date('fechanacimiento');
            
            $table->integer('lenguamaterna')->nullable();
            $table->integer('serviciosmedicos')->nullable();
            $table->integer('sexo');
            $table->integer('edad')->nullable();

            $table->boolean('is_beneficiary')->default(0);
            $table->unsignedInteger('beneficiary_service_id')->nullable();
            
            $table->string('tipo_sangre');
            $table->text('enfermedad')->nullable();
            $table->text('enfermedad_otro')->nullable();
            $table->integer('idcapturista')->nullable();
            $table->integer('status')->default(0);
                
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('beneficiaries');
    }
};
