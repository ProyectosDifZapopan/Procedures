<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('history_messages', function (Blueprint $table) {
            $table->id();

            $table->string('text');
            
            $table->unsignedSmallInteger('message_motive_id');
            $table->foreign('message_motive_id')->references('id')->on('message_motives');

            // $table->unsignedSmallInteger('modify_form_id')->nullable();
            // $table->foreign('modify_form_id')->references('id')->on('modify_forms');

            $table->unsignedBigInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users');

            $table->unsignedBigInteger('request_id');
            $table->foreign('request_id')->references('id')->on('requests');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('history_messages');
    }
};
