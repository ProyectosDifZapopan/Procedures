<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class HousingRequest extends Model
{
    use HasFactory;
    protected $fillable = [
        'valor_estimado',
        'tipo_vivienda',
        'pago_mensual',
        'arrendador_propietario',

        'telefono',
        'contrato',
        'vive_con_familiares',
        'vencimiento_contrato',
        'cantidad_aporta',
        'horario_en_casa_1',
        'horario_en_casa_2',
        'request_id',
    ];

    protected $casts = [
        'created_at'  => 'date:Y-m-d',
        'updated_at' => 'datetime:Y-m-d H:00',
    ];

    public function request()
    {
        return $this->hasOne('App\Models\Requests', 'id', 'request_id');
    }

    public function address()
    {
        return $this->belongsToMany('App\Models\Address', 'address_housings', 'housing_id', 'address_id');
    }

    public function setAttribute($key, $value)
    {
        parent::setAttribute($key, $value);

        if (is_string($value))
            $this->attributes[$key] = trim(mb_strtoupper($value));
    }
}
