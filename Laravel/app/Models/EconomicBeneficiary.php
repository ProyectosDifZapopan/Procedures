<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EconomicBeneficiary extends Model
{
    use HasFactory;
    protected $fillable = [
        'ocupacion',
        'parentesco',
        'lugar_nacimiento',
        'estado_civil',
        'estado_religioso',
        'trabaja',
        'lugar_trabajo',
        'telefono_trabajo',
        'telefono_ext_trabajo',
        'jefe_inmediato',
        'codigo_trabajo',
        'colonia_trabajo',
        'municipio_trabajo',
        'entrada_trabajo',
        'salida_trabajo',
        'ingreso_mensual_bruto',
        'ingreso_mensual_neto',
        'horario_contacto_1',
        'horario_contacto_2',
        'beneficiary_id',

        'puesto',
        'antiguedad',
        'calle_trabajo',
        'numext_trabajo',
        'numint_trabajo',
        'primer_cruce_trabajo',
        'segundo_cruce_trabajo',
        'otro_trabajo',
        'colonia_name_trabajo',
        'estado_trabajo',
    ];

    protected $casts = [
        'created_at'  => 'date:Y-m-d',
        'updated_at' => 'datetime:Y-m-d H:00',
    ];

    public function beneficiary()
    {
        return $this->hasOne('App\Models\Beneficiary', 'id', 'beneficiary_id');
    }

    public function setAttribute($key, $value)
    {
        parent::setAttribute($key, $value);

        if (is_string($value))
            $this->attributes[$key] = trim(mb_strtoupper($value));
    }
}
