<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Beneficiary extends Model
{
    protected $table = 'beneficiaries';
    use HasFactory;
    protected $fillable = [
        'curp', 'nombre', 'apaterno', 'amaterno', 'fechanacimiento', 'edad', 'escolaridad', 'sexo', 'lenguamaterna', 'serviciosmedicos', 'tipo_sangre', 'enfermedad', 'enfermedad_otro', 'idcapturista', 'status', 'is_beneficiary', 'beneficiary_service_id'
    ];

    protected $casts = [
        'created_at'  => 'date:Y-m-d',
        'updated_at' => 'datetime:Y-m-d H:00',
    ];

    public function requests()
    {
        return $this->belongsToMany(Requests::class, 'beneficiary_requests', 'beneficiary_id', 'request_id');
    }

    public function creches()
    {
        return $this->belongsToMany(Creche::class, 'beneficiary_creches', 'beneficiary_id', 'creche_id');
    }

    public function employee()
    {
        return $this->belongsTo(BeneficiaryEmployee::class, 'beneficiary_employee', 'id', 'beneficiary_id');
    }

    public function beneficiaryCreche()
    {
        return $this->hasMany('App\Models\BeneficiaryCreche', 'beneficiary_id', 'id');
    }

    public function economic()
    {
        return $this->hasMany('App\Models\EconomicBeneficiary', 'beneficiary_id', 'id');
    }

    public function address()
    {
        return $this->belongsToMany('App\Models\Address', 'address_beneficiaries', 'beneficiary_id', 'address_id');
    }

    public function extraWork()
    {
        return $this->hasMany('App\Models\ExtraWork', 'beneficiary_id', 'id');
    }

    public function requestDocuments()
    {
        return $this->hasMany('App\Models\RequestDocument', 'beneficiary_id', 'id');
    }


    public function setAttribute($key, $value)
    {
        parent::setAttribute($key, $value);

        if (is_string($value))
            $this->attributes[$key] = trim(mb_strtoupper($value));
    }
}
