<?php

namespace App\Http\Controllers;

use App\Models\Log;
use App\Models\Quote;
use Illuminate\Http\Request;
use Illuminate\Mail\Message;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class QuoteController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $user = Auth::user();
        if ($user->role_id == 1) {
            return;
        }
        $model = Quote::query();
        // $model->where('procedure_id', $user->department_id)->where('status','<>',0);
        // ($user->department_id == 2 ? $model->with('centro.sala') : null);
        // ($user->department_id == 3 ? $model->with('centro.sala') : null);
        // ($user->department_id == 4 ? $model->with('centro.sala') : null);
        // ($user->department_id == 5 ? $model->with('centro.sala') : null);
        // ($user->department_id == 6 ? $model->with('centro.sala') : null);
        // ($user->profile_id == 2 ? $model->whereHas('user', function ($query) use ($user) {
        //     return  $query->where('location_id', $user->location_id);
        // }) : null);

        $query = $model
            ->has('request.beneficiaries')->orderBy('date', 'asc')->orderBy('hour', 'asc')->withCount([
                'request as beneficiaries_count' => function ($query) {
                    $query->select(DB::raw('count(*)'))
                        ->join('beneficiary_requests', 'requests.id', '=', 'beneficiary_requests.request_id')
                        ->join('beneficiaries', 'beneficiaries.id', '=', 'beneficiary_requests.beneficiary_id')
                        ->join('beneficiary_creches', 'beneficiaries.id', '=', 'beneficiary_creches.beneficiary_id')
                        ->where('beneficiary_creches.status', 1);
                }
            ])
            ->has('request.crecheRequest')
            ->with(['request.crecheRequest.degree', 'request.priority', 'request.beneficiaries' => function ($query) {
                $query->orderBy('id', 'asc');
            }])->whereHas('request', function ($query) use ($user) {
                $query->where('procedure_id', $user->department_id)->where('center_id', $user->center_id);
            })->paginate(10);
        return response()->json($query);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $user = Auth::user();

        if ($user->role_id == 1) {
            return;
        }

        $body = $request->all();
        $quote = Quote::where('request_id', $request->request_id)->where('attended', '<>', 0)->first();

        if ($quote == null) {
            $quote = Quote::create($body);

            $email = $quote->request->user->email;
            $label = 'Se a creado una cita para el día ' . $quote->date . ' a las ' . $quote->hour . ' hrs. de la solicitud No.' . $request->request_id . '.';
            $cita = 'El estado de la cita es: "' . ($quote->attended == 2 ? 'PENDIENTE POR ASISTIR' : ($quote->attended == 1 ? 'ASISTIO' : 'NO ASISTIO')) . '".';

            Mail::send('emails.quote-generated', [
                'name' => $quote->request->user->name,
                'request' => $quote,
                'label' => $label,
                'quote' => $cita
            ], function (Message $message) use ($email) {
                $message->to($email)
                    ->subject('Cita Agendada');
            });

            Log::create([
                'user_id' => auth()->id(), // o null si el usuario no está autenticado
                'receiver_id' => $quote->request->user->id,
                'request_id' => $quote->request->id,
                'action' => 'Cita creada',
                'description' => 'Cita creada para la solicitud con folio No.' . $quote->request->invoice . ', con fecha ' . $quote->date . ' y horario de ' . $quote->hour . '.',
                'status' => 1,
                'read' => 0
            ]);


            $response['message'] = "Cita registrada correctamente.";
            $response['code'] = 200;
        } else {
            $response['message'] = "Ya existe una Cita para esta Solicitud.";
            $response['code'] = 202;
        }

        return response()->json($response);
    }

    /**
     * Display the specified resource.
     */
    public function show(Quote $quote)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Quote $quote)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request)
    {
        $query = Quote::find($request->id);
        $query->update([
            'attended' => $request->attended
        ]);

        if ($request->attended == 1) {

            $response['code'] = 200;
            $response['message'] = "Asistencia registrada correctamente.";
        } elseif ($request->attended == 0) {

            $response['code'] = 200;
            $response['message'] = "Falta registrada correctamente.";
        }

        $email = $query->request->user->email;
        $label = 'La cita del día ' . $query->date . ' a las ' . $query->hour . ' hrs. de la solicitud No.' . $request->id . '.';
        $cita = 'El estado registrado en la cita es: "' . ($query->attended == 2 ? 'PENDIENTE A ASISTIR' : ($query->attended == 1 ? 'ASISTIO' : 'NO ASISTIO')) . '".';

        Mail::send('emails.attendance-record', [
            'name' => $query->request->user->name,
            'request' => $query,
            'label' => $label,
            'quote' => $cita
        ], function (Message $message) use ($email) {
            $message->to($email)
                ->subject('Estado De Solicitud Actualizado');
        });

        Log::create([
            'user_id' => auth()->id(), // o null si el usuario no está autenticado
            'receiver_id' => $query->request->user->id,
            'request_id' => $query->request->id,
            'action' => 'Cita actualizada',
            'description' => 'Nuevo estado de cita registrado como ' . ($query->attended == 2 ? 'PENDIENTE A ASISTIR' : ($query->attended == 1 ? 'ASISTIO' : 'NO ASISTIO')) . '.',
            'status' => 0,
            'read' => 0
        ]);

        return response()->json($response);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Quote $quote)
    {
        //
    }

    public function search($value)
    {

        $param = explode(":", $value);
        $user = Auth::user();
        if ($user->role_id == 1) {
            return;
        }
        $model = Quote::query();

        ($param[0] == 'attended' ? $model->where('attended', $param[1]) : null);

        $model->has('request.beneficiaries')->orderBy('date', 'asc')->orderBy('hour', 'asc')->withCount([
            'request as beneficiaries_count' => function ($query) {
                $query->select(DB::raw('count(*)'))
                    ->join('beneficiary_requests', 'requests.id', '=', 'beneficiary_requests.request_id')
                    ->join('beneficiaries', 'beneficiaries.id', '=', 'beneficiary_requests.beneficiary_id')
                    ->join('beneficiary_creches', 'beneficiaries.id', '=', 'beneficiary_creches.beneficiary_id')
                    ->where('beneficiary_creches.status', 1);
            }
        ])
            ->has('request.crecheRequest')
            ->with(['request.crecheRequest.degree', 'request.priority', 'request.beneficiaries' => function ($query) {
                $query->orderBy('id', 'asc');
            }])->whereHas('request', function ($query) use ($user) {
                $query->where('procedure_id', $user->department_id)->where('center_id', $user->center_id);
            });

        $query = $model->paginate(10);
        return response()->json($query);
    }

    public function searchValue($value)
    {
        $user = Auth::user();
        if ($user->role_id == 1) {
            return;
        }
        $model = Quote::query();

        $model->has('request.beneficiaries')->orderBy('date', 'asc')->orderBy('hour', 'asc')->withCount([
            'request as beneficiaries_count' => function ($query) {
                $query->select(DB::raw('count(*)'))
                    ->join('beneficiary_requests', 'requests.id', '=', 'beneficiary_requests.request_id')
                    ->join('beneficiaries', 'beneficiaries.id', '=', 'beneficiary_requests.beneficiary_id')
                    ->join('beneficiary_creches', 'beneficiaries.id', '=', 'beneficiary_creches.beneficiary_id')
                    ->where('beneficiary_creches.status', 1);
            }
        ])
            ->has('request.crecheRequest')
            ->with(['request.crecheRequest.degree', 'request.priority', 'request.beneficiaries' => function ($query) {
                $query->orderBy('id', 'asc');
            }])->whereHas('request', function ($query) use ($user) {
                $query->where('procedure_id', $user->department_id)->where('center_id', $user->center_id);
            });

        //REALIZAR BUSQUEDA 
        $query = $model->where(function ($query) use ($value) {
            $query->whereHas('request', function ($query) use ($value) {
                $query->where('invoice', 'like', '%' . $value . '%');
            })
                ->orWhereHas('request.beneficiaries', function ($query) use ($value) {
                    $query->whereRaw("CONCAT(nombre, ' ', apaterno, ' ', amaterno) LIKE ?", ['%' . $value . '%']);
                });
        })
            ->paginate(10);

        return response()->json($query);
    }
}
