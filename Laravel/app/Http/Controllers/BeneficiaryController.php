<?php

namespace App\Http\Controllers;

use App\Models\Address;
use App\Models\AddressBeneficiary;
use App\Models\Beneficiary;
use App\Models\BeneficiaryCreche;
use App\Models\BeneficiaryRequest;
use App\Models\Creche;
use App\Models\CrecheRequest;
use App\Models\Degree;
use App\Models\EconomicBeneficiary;
use App\Models\ExtraWork;
use App\Models\HousingRequest;
use App\Models\Log;
use App\Models\ModifyForm;
use App\Models\RequestDocument;
use App\Models\Requests;
use App\Models\Room;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Mail\Message;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;

class BeneficiaryController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $user = Auth::user();
        $model = Beneficiary::query();
        $query = $model->has('beneficiaryCreche')->whereHas('beneficiaryCreche.creche', function ($query) use ($user) {
            $query->where('center_id', $user->center_id);
        })
            ->with('beneficiaryCreche.creche', 'beneficiaryCreche.creche.room', 'beneficiaryCreche.creche.degree')
            ->paginate(10);
        return response()->json($query);
    }

    public function beneficiaryCreche(Request $request)
    {
        $user = Auth::user();

        if ($user->role_id == 1) {
            return;
        }

        $quote = Creche::where('id', $request->creche_id)->get();
        // return response()->json($quote);
        if (!$quote->isEmpty() && $quote[0]->capacity > $request->quota) {
            DB::beginTransaction();
            try {
                BeneficiaryCreche::create([
                    'creche_id' => $request->creche_id,
                    'beneficiary_id' => $request->beneficiary_id,
                    'status' => 1
                ]);
                CrecheRequest::find($request->pivote_id)->update([
                    'creche_id' => $request->creche_id
                ]);

                $beneficiary = Beneficiary::with('requests.user')->where('id', $request->beneficiary_id)->first();

                // return response()->json($beneficiary);
                $creche = Creche::find($request->creche_id);

                $email = $beneficiary->requests[0]->user->email;
                $name = $beneficiary->requests[0]->user->name;

                if ($email !== null) {
                    Mail::send('emails.assigned-creche', [
                        'name' => $name,
                        'beneficiary' => $beneficiary,
                        'creche' => $creche,
                    ], function (Message $message) use ($email) {
                        $message->to($email)
                            ->subject('Guarderia Asignada');
                    });

                    Log::create([
                        'user_id' => auth()->id(), // o null si el usuario no está autenticado
                        'receiver_id' => $beneficiary->requests[0]->user->id,
                        'request_id' => $beneficiary->requests[0]->id,
                        'beneficiary_id' => $beneficiary->id,
                        'action' => 'Guardeia asignada',
                        'description' => 'Beneficiario ' . $beneficiary->id . ' de la solicitud con No. ' . $beneficiary->requests[0]->invoice . ' dado de alta en la guarderia ' . $creche->center->name . ' sala ' . $creche->room->name . '.',
                        'status' => 1,
                        'read' => 0,
                    ]);
                } else {
                    Log::create([
                        'user_id' => auth()->id(), // o null si el usuario no está autenticado
                        'beneficiary_id' => $beneficiary->id,
                        'action' => 'Guardeia asignada',
                        'description' => 'Beneficiario ' . $beneficiary->id . ' dado de alta en la guarderia ' . $creche->center->name . '-' . $creche->room->name . '.',
                        'status' => 0,
                        'read' => 0,
                    ]);
                }

                DB::commit();
                $response['message'] = "Beneficiario registrado correctamente en la guarderia";
                $response['code'] = 200;
            } catch (\Throwable $th) {
                DB::rollBack();
                $response['message'] = "Guarderia no se pudo asignar";
                $response['code'] = 202;
            }
        } else {
            $response['message'] = "Guarderia sin cupos";
            $response['code'] = 202;
        }

        return response()->json($response);
    }

    public function updateBeneficiaryCreche(Request $request)
    {
        // return response()->json($request->all());
        $user = Auth::user();
        if ($user->role_id == 1) {
            return;
        }

        $quote = Creche::find($request->creche_id);
        $beneficiaryCreche = BeneficiaryCreche::find($request->pivote_id);

        if (isset($request->status)) {
            if ($request->status == 0) {
                $beneficiaryCreche->update([
                    'status' => $request->status
                ]);

                $beneficiary = Beneficiary::with('requests.user')->where('id', $beneficiaryCreche->beneficiary_id)->first();
                $create = Creche::find($beneficiaryCreche->creche_id);

                $email = $beneficiary->requests[0]?->user?->email ?? null;
                $name = $beneficiary->requests[0]?->user?->name ?? null;
                $telefono = $create->center->telefono;

                $newLabel = $beneficiaryCreche->status;

                if ($email !== null) {
                    Mail::send('emails.change-status-creche', [
                        'name' => $name,
                        'beneficiary' => $beneficiary,
                        'newLabel' => $newLabel,
                        'telefono' => $telefono
                    ], function (Message $message) use ($email) {
                        $message->to($email)
                            ->subject('Estado De Guarderia');
                    });
                    Log::create([
                        'user_id' => auth()->id(), // o null si el usuario no está autenticado
                        'receiver_id' => $beneficiary->requests[0]?->user?->id,
                        'request_id' => $beneficiary->requests[0]->id,
                        'beneficiary_id' => $beneficiary->id,
                        'action' => 'Baja De Guarderia',
                        'description' => 'Se ha dado de baja el beneficiario ' . $beneficiary->id . '.',
                        'status' => 1,
                        'read' => 0
                    ]);
                } else {
                    Log::create([
                        'user_id' => auth()->id(), // o null si el usuario no está autenticado
                        'beneficiary_id' => $beneficiary->id,
                        'action' => 'Baja De Guarderia',
                        'description' => 'Se ha dado de baja el beneficiario ' . $beneficiary->id . '.',
                        'status' => 0,
                        'read' => 0
                    ]);
                }

                $response['message'] = "Beneficiario dado de baja correctamente";
                $response['code'] = 200;
            } else if ($quote && $quote->capacity > $request->quota && $request->status == 1) {
                $beneficiaryCreche->update([
                    'creche_id' => $request->creche_id,
                    'status' => $request->status
                ]);

                $beneficiary = Beneficiary::with('requests.user')->where('id', $beneficiaryCreche->beneficiary_id)->first();
                $create = Creche::find($beneficiaryCreche->creche_id);

                $email = $beneficiary->requests[0]?->user?->email ?? null;
                $name = $beneficiary->requests[0]?->user?->name ?? null;
                $telefono = $create->center->telefono;

                $newLabel = $beneficiaryCreche->status;
                $creche = Creche::find($request->creche_id);
                if ($email != null) {
                    Mail::send('emails.change-status-creche', [
                        'name' => $name,
                        'beneficiary' => $beneficiary,
                        'newLabel' => $newLabel,
                        'telefono' => $telefono
                    ], function (Message $message) use ($email) {
                        $message->to($email)
                            ->subject('Estado De Guarderia');
                    });

                    if ($email !== null) {
                        Mail::send('emails.assigned-creche', [
                            'name' => $name,
                            'beneficiary' => $beneficiary,
                            'creche' => $creche,
                        ], function (Message $message) use ($email) {
                            $message->to($email)
                                ->subject('Guarderia Asignada');
                        });
                    }
                    Log::create([
                        'user_id' => auth()->id(), // o null si el usuario no está autenticado
                        'receiver_id' => $beneficiary->requests[0]?->user?->id,
                        'request_id' => $beneficiary->requests[0]->id,
                        'beneficiary_id' => $beneficiary->id,
                        'action' => 'Cambio De Guarderia',
                        'description' => 'Se ha cambiado la sala de guarderia del beneficiario ' . $beneficiary->id . ', la nueva guarderia es ' . $creche->center->name . ' de la sala ' . $creche->room->name . '.',
                        'status' => 1,
                        'read' => 0
                    ]);
                } else {
                    Log::create([
                        'user_id' => auth()->id(), // o null si el usuario no está autenticado
                        'beneficiary_id' => $beneficiary->id,
                        'action' => 'Cambio De Guarderia',
                        'description' => 'Se ha cambiado la sala de guarderia del beneficiario ' . $beneficiary->id . ', la nueva guarderia es ' . $creche->center->name . ' de la sala ' . $creche->room->name . '.',
                        'status' => 0,
                        'read' => 0
                    ]);
                }

                $response['message'] = "Beneficiario registrado correctamente en la guarderia";
                $response['code'] = 200;
            }
        } else if ($quote && $quote->capacity > $request->quota) {
            $beneficiaryCreche->update([
                'creche_id' => $request->creche_id,
            ]);

            $creche = Creche::find($request->creche_id);

            $beneficiary = Beneficiary::with('requests.user')->where('id', $request->beneficiary_id)->first();
            $email = $beneficiary->requests[0]?->user?->email ?? null;
            $name = $beneficiary->requests[0]?->user?->name ?? null;

            $label = 'La nueva Guarderia es: ' . $creche->center->name . '.';

            if ($email !== null) {
                Mail::send('emails.change-status-creche', [
                    'name' => $name,
                    'beneficiary' => $beneficiary,
                    'label' => $label,
                    'creche' => $creche
                ], function (Message $message) use ($email) {
                    $message->to($email)
                        ->subject('Cambio De Guarderia');
                });

                Log::create([
                    'user_id' => auth()->id(), // o null si el usuario no está autenticado
                    'receiver_id' => $beneficiary->requests[0]?->user?->id,
                    'request_id' => $beneficiary->requests[0]->id,
                    'beneficiary_id' => $beneficiary->id,
                    'action' => 'Cambio De Guarderia',
                    'description' => 'Se ha cambiado la sala de guarderia del beneficiario ' . $beneficiary->id . ', la nueva guarderia es ' . $creche->center->name . ' de la sala ' . $creche->room->name . '.',
                    'status' => 1,
                    'read' => 0,
                ]);
            } else {
                Log::create([
                    'user_id' => auth()->id(), // o null si el usuario no está autenticado
                    'beneficiary_id' => $beneficiary->id,
                    'action' => 'Cambio De Guarderia',
                    'description' => 'Se ha cambiado la sala de guarderia del beneficiario ' . $beneficiary->id . ', la nueva guarderia es ' . $creche->center->name . ' de la sala ' . $creche->room->name . '.',
                    'status' => 0,
                    'read' => 0
                ]);
            }

            $response['message'] = "Beneficiario cambiado correctamente de guardería";
            $response['code'] = 200;
        } else {
            $response['message'] = "Guardería sin cupos";
            $response['code'] = 202;
        }
        return response()->json($response);
    }

    public function changeCenter(Request $request)
    {
        // return response()->json($request->all());
        $user = Auth::user();
        if ($user->role_id == 1) {
            return;
        }
        $beneficiaryCreche = BeneficiaryCreche::find($request->pivote_id);

        $beneficiaryCreche->update([
            'creche_id' => $request->new, 'beneficiary_id', 'status'
        ]);

        //Log pendiente

        $response['message'] = "Guardería sin cupos";
        $response['code'] = 202;

        return response()->json($response);
    }


    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function parentsStore(Request $request)
    {
        $user = Auth::user();
        // return response()->json($request->all());
        $body = $request->all();
        $requests = Requests::where('id', $request->request_id)->where('user_id', $user->id)->get();

        if (!$requests->isEmpty()) {
            DB::beginTransaction();
            try {
                $newBeneficiary = Beneficiary::create($body);

                if ($body['address_id'] == null) {
                    $newAddress = Address::create($body);
                    AddressBeneficiary::create([
                        'beneficiary_id' => $newBeneficiary->id,
                        'address_id' => $newAddress->id,
                    ]);
                } else {
                    AddressBeneficiary::create([
                        'beneficiary_id' => $newBeneficiary->id,
                        'address_id' => $body['address_id'],
                    ]);
                }

                $body['beneficiary_id'] = $newBeneficiary->id;
                $request['beneficiary_id'] = $newBeneficiary->id;
                BeneficiaryRequest::create($body);
                EconomicBeneficiary::create([
                    'ocupacion' => $request->ocupacion,
                    'parentesco' => $request->parentesco,
                    'lugar_nacimiento' => $request->lugar_nacimiento,
                    'estado_civil' => $request->estado_civil,
                    'estado_religioso' => $request->estado_religioso,
                    'trabaja' => $request->trabaja,
                    'lugar_trabajo' => $request->lugar_trabajo,
                    'calle_trabajo' => $request->calle_trabajo,
                    'telefono_trabajo' => $request->telefono_trabajo,
                    'telefono_ext_trabajo' => $request->telefono_ext_trabajo,
                    'jefe_inmediato' => $request->jefe_inmediato,
                    'codigo_trabajo' => $request->codigo_trabajo,
                    'colonia_trabajo' => $request->colonia_trabajo,
                    'colonia_name_trabajo' => $request->colonia_name_trabajo,
                    'municipio_trabajo' => $request->municipio_trabajo,
                    'estado_trabajo' => $request->estado_trabajo,
                    'entrada_trabajo' => $request->entrada_trabajo,
                    'salida_trabajo' => $request->salida_trabajo,
                    'ingreso_mensual_bruto' => $request->ingreso_mensual_bruto,
                    'ingreso_mensual_neto' => $request->ingreso_mensual_neto,
                    'beneficiary_id' => $request->beneficiary_id,

                    'puesto' => $request->puesto,
                    'antiguedad' => $request->antiguedad,
                    'numext_trabajo' => $request->numext_trabajo,
                    'numint_trabajo' => $request->numint_trabajo,
                    'primer_cruce_trabajo' => $request->primer_cruce_trabajo,
                    'segundo_cruce_trabajo' => $request->segundo_cruce_trabajo,
                    'otro_trabajo' => $request->otro_trabajo,
                ]);
                if ($request->otro_trabajo == 1) {
                    ExtraWork::create([
                        'lugar' => $request->lugar_otro_trabajo,
                        'telefono' => $request->telefono_otro_trabajo,
                        'domicilio' => $request->domicilio_otro_trabajo,
                        'jefe' => $request->jefe_otro_trabajo,
                        'entrada' => $request->entrada_otro_trabajo,
                        'salida' => $request->salida_otro_trabajo,
                        'beneficiary_id' => $request->beneficiary_id,
                    ]);
                }
                Log::create([
                    'user_id' => auth()->id(), // o null si el usuario no está autenticado
                    'request_id' => $requests[0]->id,
                    'beneficiary_id' => $newBeneficiary->id,
                    'action' => 'Tutor creado',
                    'description' => 'Tutor dado de alta para la solicitud con folio No.' . $requests[0]->invoice . '.',
                    'status' => 0,
                    'read' => 0
                ]);

                DB::commit();
                $response['message'] = "Beneficiario registradao correctamente";
                $response['code'] = 200;
            } catch (\Throwable $th) {
                DB::rollBack();
                $response['message'] = "No se ha podido crear el trabajador";
                $response['code'] = 202;
            }
        } else {
            $response['message'] = "No se ha encontrado la solicitud";
            $response['code'] = 202;
        }

        return response()->json($response);
    }
    public function store(Request $request)
    {
        $user = Auth::user();
        $body = $request->all();
        $requests = Requests::where('id', $request->request_id)->where('user_id', $user->id)->get();
        if (!$requests->isEmpty()) {
            DB::beginTransaction();
            try {
                $newBeneficiary = Beneficiary::create($body);

                BeneficiaryRequest::create([
                    'beneficiary_id' => $newBeneficiary->id,
                    'request_id' => $request->request_id,
                ]);

                Log::create([
                    'user_id' => auth()->id(), // o null si el usuario no está autenticado
                    'request_id' => $requests[0]->id,
                    'beneficiary_id' => $newBeneficiary->id,
                    'action' => 'Beneficiario creado',
                    'description' => 'Beneficiario dado de alta para la solicitud con folio No.' . $requests[0]->invoice . '.',
                    'status' =>  0,
                    'read' => 0
                ]);

                DB::commit();
                $response['beneficiary_id'] = $newBeneficiary->id;
                $response['message'] = "Beneficiario registradao correctamente";
                $response['code'] = 200;
            } catch (\Throwable $th) {
                DB::rollBack();
                $response['message'] = "No se ha encontrado la solicitud";
                $response['code'] = 202;
            }
        } else {
            $response['message'] = "No se ha encontrado la solicitud";
            $response['code'] = 202;
        }

        return response()->json($response);
    }

    public function beneficiaryService(Request $request)
    {
        // return response()->json($request->request_id);
        $user =  Auth::user();
        $bene = $request->beneficiary_id;
        $requests = Requests::findOrFail($request->request_id);
        $beneficiary = Beneficiary::findOrFail($bene);
        $housing =  HousingRequest::with('address')->whereHas('request', function ($query) use ($request) {
            return $query->where('id', $request->request_id);
        })->first();
        $tutor = Beneficiary::find($request->tutor_id);
        $usuario = DB::connection('mysql')->table('users')->where('email', $user->email)->first();
        // return response()->json($housing);
        try {
            DB::beginTransaction();
            $id = DB::connection('mysql')->table('persona_test')->insertGetId([
                // 'iddifzapopan' => '',
                'fechacaptura' => Carbon::now() ?? null,
                // 'departamentocaptura' => '',
                // 'perfilatencion' => '',
                // 'problematicadpna' => '',
                'no_curp' => 0 ?? null,
                // 'no_curp_motivo' => '',
                'curp' => $beneficiary->curp ?? null,
                'nombre' => $beneficiary->nombre ?? null,
                'apaterno' => $beneficiary->apaterno ?? null,
                'amaterno' => $beneficiary->amaterno ?? null,
                'fechanacimiento' => $beneficiary->fechanacimiento ?? null,
                'sexo' => $beneficiary->sexo ?? null,

                // 'estadocivil' => '',
                // 'ocupacion' => '',
                // 'ocupacion_otro' => '',

                'escolaridad' => $beneficiary->escolaridad ?? null,
                'lenguamaterna' => $beneficiary->lenguamaterna ?? null,

                // 'lenguamaterna_otro' => '',
                // 'lenguasecundaria' => '',
                // 'lenguasecundaria_otro' => '',

                'serviciosmedicos' => $beneficiary->serviciosmedicos ?? null,
                // 'serviciosmedicos_otro' => $beneficiary->id ?? null,
                'enfermedad' => $beneficiary->enfermedad ?? null,
                'enfermedad_otro' => $beneficiary->enfermedad_otro ?? null,
                'calle' => $housing->address[0]->calle ?? null,
                'numext' => $housing->address[0]->numext ?? null,
                'numint' => $housing->address[0]->numint ?? null,
                'primercruce' => $housing->address[0]->primercruce ?? null,
                'segundocruce' => $housing->address[0]->segundocruce ?? null,
                'codigopostal' => $housing->address[0]->codigopostal ?? null,
                'colonia' => $housing->address[0]->colonia ?? null,
                'municipio' => $housing->address[0]->municipio ?? null,
                // 'estado' => '',
                'telefono' => $beneficiary->celular ?? null,
                // 'lat' => '',
                // 'lng' => '',
                // 'celular' => '',
                // 'escontacto' => '',
                'email' => $requests->user->email ?? null,
                'tiene_tutor' => $tutor ? 1 : 0,
                'tutorid' => $tutor->id ?? null,
                'tutorcurp' => $tutor->curp ?? null,
                'parentesco' => '' ?? null,
                'tutornombre' => $tutor->nombre ?? null,
                'tutorapaterno' => $tutor->apaterno ?? null,
                'tutoramaterno' => $tutor->amaterno ?? null,
                'tutorsexo' => $tutor->sexo ?? null,
                'tutorfechanacimiento' => $tutor->fechanacimiento ?? null,
                'tutorcelular' => $tutor->celular ?? null,
                'tutormismodomicilio' => null,
                'tutorcalle' => $tutor->calle ?? null,
                'tutornumext' => $tutor->numext ?? null,
                'tutornumint' => $tutor->numint ?? null,
                'tutorcodigopostal' => $tutor->codigopostal ?? null,
                'tutorcolonia' => $tutor->colonia ?? null,
                'tutormunicipio' => $tutor->municipio ?? null,
                'tutorestado' => null,

                'vivienda' => $request->housing->tipo_vivienda ?? null,
                'vivienda_otro' => null,

                'idcapturista' => $usuario->id ?? null,
                'estatus' => 1,
            ]);

            $iddifzapopan = "DIFZAP" . date("Y") . str_pad($id, 6, "0", STR_PAD_LEFT);

            DB::connection('mysql')->table('persona_test')->where('id', $id)->update(['iddifzapopan' => $iddifzapopan]);

            $beneficiary->update(['is_beneficiary' => 1, 'beneficiary_service_id' => $id]);

            Log::create([
                'user_id' => auth()->id(), // o null si el usuario no está autenticado
                'request_id' => $requests->id,
                'beneficiary_id' => $beneficiary->id,
                'action' => 'Beneficiario Registrado en Padrón',
                'description' => 'Beneficiario dado de alta en padrón con identificador ' . $iddifzapopan . ' con información de la solicitud con folio No.' . $requests->invoice . '.',
                'status' => 0,
                'read' => 0
            ]);
            DB::commit();
            return response()->json(['code' => 200, 'message' => 'Beneficiario registrado en Padrón correctamente.']);
        } catch (\Exception $e) {
            DB::rollBack();
            // Maneja la excepción (puede ser un problema de concurrencia)
            // Log, notifica, etc.
            return response()->json(['code' => 200, 'message' => 'No se ha podido crear el beneficiario. ' . $e]);
        }
    }

    public function beneficiaryOfService(Request $request)
    {
        $user = Auth::user();
        $beneficiary = DB::connection('mysql')->table('persona_test')->where('curp', $request['curp'])->first();
        $child = Beneficiary::where('curp', $request['curp'])->first();
        // return response()->json($beneficiary);

        if ($beneficiary) {
            try {
                if (!$child) {
                    $newBeneficiary = Beneficiary::create([
                        'curp' => $beneficiary->curp,
                        'nombre' => $beneficiary->nombre,
                        'apaterno' => $beneficiary->apaterno,
                        'amaterno' => $beneficiary->amaterno ?? '-PENDIENTE-',
                        'fechanacimiento' => $beneficiary->fechanacimiento,
                        // 'edad' => $beneficiary->edad,
                        'edad' => 100,
                        'calle' => $beneficiary->calle,
                        'numext' => $beneficiary->numext,
                        'numint' => $beneficiary->numint,
                        'escolaridad' => $beneficiary->escolaridad,
                        'primercruce' => $beneficiary->primercruce ?? '-PENDIENTE-',
                        'segundocruce' => $beneficiary->segundocruce,
                        'vivienda' => $beneficiary->vivienda,
                        'municipio' => $beneficiary->municipio,
                        'codigopostal' => $beneficiary->codigopostal,
                        'colonia' => $beneficiary->colonia,
                        'celular' => $beneficiary->celular,
                        'sexo' => $beneficiary->sexo,
                        'lenguamaterna' => $beneficiary->lenguamaterna,
                        'serviciosmedicos' => $beneficiary->serviciosmedicos,
                        // 'tipo_sangre' => $beneficiary->tipo_sangre,
                        'tipo_sangre' => '-PENDIENTE-',
                        'enfermedad' => $beneficiary->enfermedad,
                        'enfermedad_otro' => $beneficiary->enfermedad_otro,
                        'idcapturista' => $user->id,
                        'status' => 0,
                        'is_beneficiary' => 1,
                        'beneficiary_service_id' => $beneficiary->id
                    ]);

                    Log::create([
                        'user_id' => auth()->id(), // o null si el usuario no está autenticado
                        'beneficiary_id' => $newBeneficiary->id,
                        'action' => 'Beneficiario Registrado desde Padrón',
                        'description' => 'Beneficiario dado de alta con la información del padrón con identificador ' . $beneficiary->iddifzapopan . '.',
                        'status' => 0,
                        'read' => 0
                    ]);

                    return response()->json(['code' => 200, 'message' => 'Beneficiario registrado desde Padrón correctamente.']);
                } else {
                    return response()->json(['code' => 200, 'message' => 'Beneficiario ya registrado.']);
                }
            } catch (\Illuminate\Database\QueryException $e) {
                // Puedes manejar específicamente las excepciones de consulta de Laravel
                // Log, notifica, etc.
                return response()->json(['code' => 200, 'message' => 'No se ha podido crear el beneficiario. ' . $e->getMessage()]);
            } catch (\Exception $e) {
                // Otras excepciones
                // Log, notifica, etc.
                return response()->json(['code' => 200, 'message' => 'Error desconocido. ' . $e->getMessage()]);
            }
        } else {
            return response()->json(['code' => 201, 'message' => 'La CURP no coincide con ningún beneficiario.']);
        }
    }

    /**
     * Display the specified resource.
     */
    public function show($id)
    {
        $user = Auth::user();
        if ($user->role_id == 1) {
            $rule = Beneficiary::where('id', $id)->whereHas('requests', function ($query) use ($user) {
                $query->where('user_id', $user->id);
            })->get();

            if ($rule->isEmpty()) {
                $response['message'] = "Beneficiario no encontrado";
                $response['code'] = 404;
                return response()->json($response);
            }
        }
        $query = Beneficiary::query();
        $beneficiary = $query->where('id', $id)->with('economic', 'requests.modify_forms')->first();
        return response()->json($beneficiary);
    }

    public function uploadDocuments(Request $request)
    {

        $requests = Requests::find($request->request_id);
        $beneficiary = Beneficiary::find($request->beneficiary_id);

        // Obtener el archivo de la solicitud
        $front_ine = $request->file('front_ine');
        $behind_ine = $request->file('behind_ine');

        // Obtener la extensión de los archivos
        $frontExtension = $front_ine->getClientOriginalExtension();
        $behindExtension = $behind_ine->getClientOriginalExtension();

        // Personalizar el nombre del archivo, incluyendo la extensión
        $customFileName = 'ine_' . $requests->invoice . '_' . $request->beneficiary_id . '_flontal.' . $frontExtension;
        $customFile2Name = 'ine_' . $requests->invoice . '_' . $request->beneficiary_id . '_trasera.' . $behindExtension;

        // Guardar los archivos en el disco 'ine'
        $filePath = Storage::disk('ine')->put($customFileName, file_get_contents($front_ine));
        $filePath2 = Storage::disk('ine')->put($customFile2Name, file_get_contents($behind_ine));

        if ($filePath == 1 && $filePath2 == 1) {
            // Aquí puedes guardar la información en la base de datos si es necesario
            RequestDocument::create([
                'required_document_id' => 1,
                'request_id' => $request->request_id,
                'beneficiary_id' => $request->beneficiary_id,
                'url' => $customFileName
            ]);

            RequestDocument::create([
                'required_document_id' => 1,
                'request_id' => $request->request_id,
                'beneficiary_id' => $request->beneficiary_id,
                'url' => $customFile2Name
            ]);

            Log::create([
                'user_id' => auth()->id(), // o null si el usuario no está autenticado
                'request_id' => $requests->id,
                'beneficiary_id' => $beneficiary->id,
                'action' => 'Documentos subidos',
                'description' => 'Documentos de identificación subidos del beneficiario ' . $beneficiary->id . ' de la solicitud con No. ' . $requests->invoice . '.',
                'status' => 0,
                'read' => 0
            ]);

            return response()->json(['message' => 'Archivos subidos exitosamente.', 'code' => 200]);
        } else {
            return response()->json(['message' => 'No se pudo subir los archivos.', 'code' => 201]);
        }
    }

    public function uploadDocumentsCisz(Request $request)
    {

        $requests = Requests::find($request->request_id);
        $beneficiary = Beneficiary::find($request->beneficiary_id);

        // Obtener el archivo de la solicitud
        $employee_id = $request->file('employee_id');
        $document_cisz = $request->file('document_cisz');

        // Obtener la extensión de los archivos
        $frontExtension = $employee_id->getClientOriginalExtension();
        $behindExtension = $document_cisz->getClientOriginalExtension();

        if ($behindExtension == 'pdf' || $behindExtension == 'docx' ||  $behindExtension == 'doc') {
            if ($frontExtension == 'png' ||  $frontExtension == 'jpg' ||  $frontExtension == 'jpeg') {

                // Verificar el tamaño de los archivos
                $maxFileSize = 3 * 1024 * 1024; // 3MB (cambiar según tus necesidades)
                if ($employee_id->getSize() > $maxFileSize) {
                    return response()->json(['message' => 'El tamaño del archivo "identificación" es demasiado grande.', 'code' => 201]);
                }
                if ($document_cisz->getSize() > $maxFileSize) {
                    return response()->json(['message' => 'El tamaño del archivo "carta intención" es demasiado grande.', 'code' => 201]);
                }

                // Personalizar el nombre del archivo, incluyendo la extensión
                $customFileName = 'id_empleado_' . $requests->invoice . '_' . $request->beneficiary_id . '.' . $frontExtension;
                $customFile2Name = 'cisz_documento_' . $requests->invoice . '_' . $request->beneficiary_id . '.' . $behindExtension;

                // Guardar los archivos en el disco 'documentos_cisz'
                $filePath = Storage::disk('documentos_cisz')->put($customFileName, file_get_contents($employee_id));
                $filePath2 = Storage::disk('documentos_cisz')->put($customFile2Name, file_get_contents($document_cisz));

                if ($filePath == 1 && $filePath2 == 1) {
                    DB::beginTransaction();
                    try {
                        // Aquí puedes guardar la información en la base de datos si es necesario
                        RequestDocument::create([
                            'required_document_id' => 2,
                            'request_id' => $request->request_id,
                            'beneficiary_id' => $request->beneficiary_id,
                            'url' => $customFile2Name
                        ]);

                        Log::create([
                            'user_id' => auth()->id(), // o null si el usuario no está autenticado
                            'request_id' => $requests->id,
                            'beneficiary_id' => $beneficiary->id,
                            'action' => 'Documento subido',
                            'description' => 'Documento CISZ del beneficiario ' . $beneficiary->id . ' de la solicitud con No. ' . $requests->invoice . '.',
                            'status' => 0,
                            'read' => 0
                        ]);

                        RequestDocument::create([
                            'required_document_id' => 3,
                            'request_id' => $request->request_id,
                            'beneficiary_id' => $request->beneficiary_id,
                            'url' => $customFileName
                        ]);

                        Log::create([
                            'user_id' => auth()->id(), // o null si el usuario no está autenticado
                            'request_id' => $requests->id,
                            'beneficiary_id' => $beneficiary->id,
                            'action' => 'Documento subido',
                            'description' => 'Documento de identificación de empleado ' . $beneficiary->id . ' de la solicitud con No. ' . $requests->invoice . '.',
                            'status' => 0,
                            'read' => 0
                        ]);

                        DB::commit();

                        return response()->json(['message' => 'Archivos subidos exitosamente.', 'code' => 200]);
                    } catch (\Throwable $th) {
                        DB::rollBack();
                        return response()->json(['message' => 'No se pudo subir los archivos.', 'code' => 201]);
                    }
                } else {
                    return response()->json(['message' => 'No se pudo subir los archivos.', 'code' => 201]);
                }
            } else {
                return response()->json(['message' => 'Verifica que la imagen sea un formato valido.', 'code' => 201]);
            }
        } else {
            return response()->json(['message' => 'Verifica que el formato del documento sea valido.', 'code' => 201]);
        }
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Beneficiary $beneficiary)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function patch(Request $request, $id, $form_id = null)
    {
        $user = Auth::user();
        $body = $request->all();

        try {
            $beneficiary = Beneficiary::find($id);

            // return response()->json($form_id);
            if ($beneficiary) {
                DB::beginTransaction();
                try {
                    /// Actualiza los datos específicos del beneficiario
                    $beneficiary->update($body);

                    if ($form_id) {
                        // El parámetro form_id está presente en la URL
                        $modify = ModifyForm::find($form_id);
                        $modify->update(['status' => 1]);
                        $message = 'Beneficiario actualizado correctamente por motivos de corrección, identificador de peteción ' . $form_id . '.';
                    } else {
                        // El parámetro form_id no está presente en la URL
                        $message = 'Beneficiario actualizado correctamente.';
                    }

                    // Actualiza cualquier otra lógica que necesites
                    Log::create([
                        'user_id' => auth()->id(), // o null si el usuario no está autenticado
                        'beneficiary_id' => $id,
                        'action' => 'Beneficiario Actualizado',
                        'description' => $message,
                        'status' => 0,
                        'read' => 0
                    ]);
                    DB::commit();

                    $response['beneficiary_id'] = $beneficiary->id;
                    $response['message'] = "Beneficiario actualizado correctamente";
                    $response['code'] = 200;
                } catch (\Throwable $th) {
                    DB::rollBack();
                    $response['message'] = "Beneficiario no actualizado";
                    $response['code'] = 404;
                }
            } else {
                $response['message'] = "Beneficiario no encontrado";
                $response['code'] = 404;
            }

            return response()->json($response);
        } catch (\Illuminate\Database\QueryException $e) {
            // Puedes manejar específicamente las excepciones de consulta de Laravel
            // Log, notifica, etc.
            return response()->json(['code' => 200, 'message' => 'No se ha podido crear el beneficiario. ' . $e->getMessage()]);
        } catch (\Exception $e) {
            // Otras excepciones
            // Log, notifica, etc.
            return response()->json(['code' => 200, 'message' => 'Error desconocido. ' . $e->getMessage()]);
        }
    }

    /**
     * Update the specified resource in storage.
     */
    public function patchParent(Request $request, $id, $form_id = null)
    {
        $user = Auth::user();
        $body = $request->all();
        try {
            $beneficiary = Beneficiary::find($id);

            // return response()->json($form_id);
            if ($beneficiary) {
                DB::beginTransaction();
                // Actualiza los datos específicos del beneficiario
                $beneficiary->update($body);

                if ($body['address_id'] == null) {
                    $newAddress = Address::create($body);
                    AddressBeneficiary::create([
                        'beneficiary_id' => $beneficiary->id,
                        'address_id' => $newAddress->id,
                    ]);
                } else {
                    $address = Address::find($body['address_id']);
                    $address->update($body);

                    $addressExist = AddressBeneficiary::where('beneficiary_id', $beneficiary->id)->where('address_id', $body['address_id'])->first();
                    if (!$addressExist) {
                        AddressBeneficiary::create([
                            'beneficiary_id' => $beneficiary->id,
                            'address_id' => $body['address_id'],
                        ]);
                    }
                }

                EconomicBeneficiary::where('beneficiary_id', $beneficiary->id)->update([
                    'ocupacion' => $request->ocupacion,
                    'parentesco' => $request->parentesco,
                    'lugar_nacimiento' => $request->lugar_nacimiento,
                    'estado_civil' => $request->estado_civil,
                    'estado_religioso' => $request->estado_religioso,
                    'trabaja' => $request->trabaja,
                    'lugar_trabajo' => $request->lugar_trabajo,
                    'calle_trabajo' => $request->calle_trabajo,
                    'telefono_trabajo' => $request->telefono_trabajo,
                    'telefono_ext_trabajo' => $request->telefono_ext_trabajo,
                    'jefe_inmediato' => $request->jefe_inmediato,
                    'codigo_trabajo' => $request->codigo_trabajo,
                    'colonia_trabajo' => $request->colonia_trabajo,
                    'colonia_name_trabajo' => $request->colonia_name_trabajo,
                    'municipio_trabajo' => $request->municipio_trabajo,
                    'estado_trabajo' => $request->estado_trabajo,
                    'entrada_trabajo' => $request->entrada_trabajo,
                    'salida_trabajo' => $request->salida_trabajo,
                    'ingreso_mensual_bruto' => $request->ingreso_mensual_bruto,
                    'ingreso_mensual_neto' => $request->ingreso_mensual_neto,
                    'beneficiary_id' => $beneficiary->id,

                    'puesto' => $request->puesto,
                    'antiguedad' => $request->antiguedad,
                    'numext_trabajo' => $request->numext_trabajo,
                    'numint_trabajo' => $request->numint_trabajo,
                    'primer_cruce_trabajo' => $request->primer_cruce_trabajo,
                    'segundo_cruce_trabajo' => $request->segundo_cruce_trabajo,
                    'otro_trabajo' => $request->otro_trabajo,
                ]);
                if ($request->otro_trabajo == 1) {
                    ExtraWork::where('beneficiary_id', $beneficiary->id)->update([
                        'lugar' => $request->lugar_otro_trabajo,
                        'telefono' => $request->telefono_otro_trabajo,
                        'domicilio' => $request->domicilio_otro_trabajo,
                        'jefe' => $request->jefe_otro_trabajo,
                        'entrada' => $request->entrada_otro_trabajo,
                        'salida' => $request->salida_otro_trabajo,
                        'beneficiary_id' => $beneficiary->id,
                    ]);
                }


                if ($form_id) {
                    // El parámetro form_id está presente en la URL
                    $modify = ModifyForm::find($form_id);
                    $modify->update(['status' => 1]);
                    $message = 'Tutor actualizado correctamente por motivos de corrección, identificador de peteción ' . $form_id . '.';
                } else {
                    // El parámetro form_id no está presente en la URL
                    $message = 'Tutor actualizado correctamente.';
                }

                // Actualiza cualquier otra lógica que necesites
                Log::create([
                    'user_id' => auth()->id(), // o null si el usuario no está autenticado
                    'beneficiary_id' => $id,
                    'action' => 'Tutor Actualizado',
                    'description' => $message,
                    'status' => 0,
                    'read' => 0
                ]);
                DB::commit();

                $response['beneficiary_id'] = $beneficiary->id;
                $response['message'] = "Beneficiario actualizado correctamente";
                $response['code'] = 200;
            } else {
                $response['message'] = "Beneficiario no encontrado";
                $response['code'] = 404;
            }

            return response()->json($response);
        } catch (\Illuminate\Database\QueryException $e) {
            // Puedes manejar específicamente las excepciones de consulta de Laravel
            // Log, notifica, etc.
            return response()->json(['code' => 200, 'message' => 'No se ha podido crear el beneficiario. ' . $e->getMessage()]);
        } catch (\Exception $e) {
            // Otras excepciones
            // Log, notifica, etc.
            return response()->json(['code' => 200, 'message' => 'Error desconocido. ' . $e->getMessage()]);
        }
    }

    public function search($value)
    {
        $user = Auth::user();
        $model = Beneficiary::query();

        $param = explode(":", $value);

        $model->has('beneficiaryCreche')->whereHas('beneficiaryCreche.creche', function ($query) use ($user) {
            $query->where('center_id', $user->center_id);
        });

        ($param[0] == 'room_id' || $param[0] == 'degree_id' ? $model->whereHas('beneficiaryCreche.creche', function ($query) use ($param){
            $query->where($param[0],$param[1]);
        }): null);

        ($param[0] == 'status' ? $model->whereHas('beneficiaryCreche', function ($query) use ($param){
            $query->where('status',$param[1]);
        } ) : null);

        $query = $model->with('beneficiaryCreche.creche', 'beneficiaryCreche.creche.room', 'beneficiaryCreche.creche.degree')
            ->paginate(10);
        return response()->json($query);
    }

    public function searchValue($value)
    {
        $user = Auth::user();
        $model = Beneficiary::query();
        $query = $model->has('beneficiaryCreche')->whereHas('beneficiaryCreche.creche', function ($query) use ($user) {
            $query->where('center_id', $user->center_id);
        })
            ->with('beneficiaryCreche.creche', 'beneficiaryCreche.creche.room', 'beneficiaryCreche.creche.degree')
            ->paginate(10);
        return response()->json($query);
    }

    public function getDegreeRoom(){
        $user = Auth::user()->center_id;

        $degree = Degree::all();
        $rooms = Room::whereHas('creches', function ($query) use ($user){
            $query->where('center_id',$user);
        })->get();

        return response()->json(['degree' => $degree, 'rooms' => $rooms]);
    }
}
