<?php

namespace App\Http\Controllers;

use App\Models\Address;
use App\Models\AddressHousing;
use App\Models\Beneficiary;
use App\Models\BeneficiaryEmployee;
use App\Models\Center;
use App\Models\CrecheRequest;
use App\Models\HousingRequest;
use App\Models\Log;
use App\Models\Procedure;
use App\Models\ReferencesRequest;
use App\Models\RequestDocument;
use App\Models\Requests;
use App\Models\StatusRequest;
use App\Models\VehicleRequest;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Mail\Message;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class RequestsController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $user = Auth::user();

        // Usuarios con rol 1 no pueden consultar
        if ($user->role_id == 1) {
            return;
        }

        $model = Requests::query();

        // Roles admin centro y admin trámite solo pueden ver solicitudes de su trámite en específico
        if ($user->role_id == 2 || $user->role_id == 3) {
            $model->where('procedure_id', $user->department_id);
        }

        // No se muestran las solicitudes sin terminar o canceladas
        $model->whereNotIn('status_request_id', [1, 6]);

        // Mostrar las solicitudes de modificación
        $model->with('modify_forms');

        // Admin de centro, solo ver solicitudes de su centro
        if ($user->role_id == 2) {
            $model->where('center_id', $user->center_id);
        }

        // Si el usuario corresponde a NIDOS, la solicitud debe de estar aplicando a una guardería
        if ($user->department_id == 1 || $user->department_id == 2) {
            $model->has('crecheRequest')->with('crecheRequest.degree');
        }

        // Las solicitudes deben de tener beneficiarios y se ordenan por edad los beneficiarios
        $query = $model->has('beneficiaries')->orderBy('id', 'asc')->with(['beneficiaries' => function ($query) {
            $query->orderBy('id', 'asc');
        }])->with('priority')->withCount('quotes')->paginate(10);

        return response()->json($query);
    }


    public function show($id)
    {
        $query = Requests::with('user', 'modify_forms.form', 'employee')->where('id', $id)->first();

        return response()->json($query);
    }

    public function getBeneficiariesRequest($request_id)
    {
        $user = Auth::user();

        $query = Beneficiary::has('requests')->whereHas('requests', function ($query) use ($request_id) {
            $query->where('requests.id', $request_id);
        })->with('economic', 'extraWork', 'address', 'employee')->get();

        return response()->json($query);
    }

    public function getParents($request_id)
    {
        $user = Auth::user();

        $query = Beneficiary::has('economic')->whereHas('requests', function ($query) use ($request_id) {
            $query->where('requests.id', $request_id);
        })->with('economic', 'extraWork', 'address')->get();

        return response()->json($query);
    }

    public function getAddress($request_id)
    {
        $user = Auth::user();

        $address = Address::whereHas('beneficiaries.requests', function ($query) use ($request_id) {
            $query->where('requests.id', $request_id);
        })->get();

        return response()->json($address);
    }

    public function getParentsRequest($request_id)
    {
        $query = Beneficiary::has('requestDocuments')->whereHas('requests', function ($query) use ($request_id) {
            $query->where('requests.id', $request_id);
        })->with('economic', 'extraWork', 'address', 'requestDocuments')->get();

        $undocumentedBeneficiary = Beneficiary::has('address')->whereDoesntHave('requestDocuments')-> // Esto asegura que no tengan documentos asociados
            whereHas('requests', function ($query) use ($request_id) {
                $query->where('requests.id', $request_id);
            })->with('economic', 'extraWork', 'address')->get();

        return response()->json(['undocumentedBeneficiary' => $undocumentedBeneficiary, 'beneficiariesComplet' => $query]);
    }

    public function showVisitorRequest()
    {
        $user = Auth::user();
        // if($user->role_id == 1){
        //     return;
        // }
        $model = Requests::query();
        $model->where('user_id', $user->id)->with('requestDocuments', 'references', 'housings', 'beneficiaries', 'quotes', 'priority', 'procedure', 'center', 'status_request', 'modify_forms.form');
        $model = $model->with('crecheRequest.degree');
        $query = $model->paginate();
        return response()->json($query);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    public function showHousing($id)
    {
        $user = Auth::user();

        if (!$user) {
            $response['message'] = "Necesitas loguearte";
            $response['code'] = 404;
            return response()->json($response);
        }

        $requests = HousingRequest::with('address')->where('request_id', $id)->get();
        $vehiculos = VehicleRequest::where('request_id', $id)->get();

        if ($requests->isEmpty()) {
            $response['message'] = "Vivienda lista para registrar";
            $response['code'] = 200;
            return response()->json($response);
        } else {
            $response = $requests->first();
        }

        return response()->json(['housing' => $response, 'vehicle' => $vehiculos]);
    }

    public function showReferences($id)
    {
        $user = Auth::user();

        if (!$user) {
            $response['message'] = "Necesitas loguearte";
            $response['code'] = 404;
            return response()->json($response);
        }

        $requests = ReferencesRequest::where('request_id', $id)->get();


        if ($requests->isEmpty()) {
            $response['message'] = "Referencias listas para registrar";
            $response['code'] = 200;
        }

        return response()->json(['references' => $requests]);
    }

    public function storeHousing(Request $request)
    {
        $user = Auth::user();

        if (!$user) {
            $response['message'] = "Necesitas loguearte";
            $response['code'] = 404;
            return response()->json($response);
        }

        $requests = Requests::where('id', $request->request_id)->where('user_id', $user->id)->first();

        if ($requests !== null) {
            $data = $request->all();
            DB::beginTransaction();
            try {
                $housing = HousingRequest::create($data);
                if ($data['address_id'] != null) {
                    Address::where('id', $data['address_id'])->update([
                        'lat' => $request->lat,
                        'lng' => $request->lng
                    ]);
                    AddressHousing::create([
                        'housing_id' => $housing->id,
                        'address_id' => $data['address_id']
                    ]);
                } else {
                    $address = Address::create($data);
                    AddressHousing::create([
                        'housing_id' => $housing->id,
                        'address_id' => $address->id
                    ]);
                }

                foreach ($request->vehiculos as $item) {
                    // Verificar que $item es un objeto antes de acceder a sus propiedades
                    if ($item['marca'] && $item['tipo'] && $item['modelo'] && $item['valor_aproximado']) {
                        $marca = $item['marca'];
                        $tipo = $item['tipo'];
                        $modelo = $item['modelo'];
                        $valor_aproximado = $item['valor_aproximado'];

                        VehicleRequest::create([
                            'marca' => $marca,
                            'tipo' => $tipo,
                            'modelo' => $modelo,
                            'valor_aproximado' => $valor_aproximado,
                            'request_id' => $request->request_id
                        ]);
                    }
                }

                Log::create([
                    'user_id' => auth()->id(), // o null si el usuario no está autenticado
                    'request_id' => $requests->id,
                    'action' => 'Vivienda creada',
                    'description' => 'Creación de vivienda de parte de usuario para la solicitud ' . $requests->invoice . '.',
                    'status' => 0,
                    'read' => 0
                ]);

                DB::commit();
                $response['message'] = "Vivienda registrada correctamente";
                $response['code'] = 200;
            } catch (\Throwable $th) {
                $response['message'] = "Vivienda no registrada";
                $response['code'] = 202;
            }
        } else {
            $response['message'] = "No existe la solicitud";
            $response['code'] = 202;
        }

        return response()->json($response);
    }

    public function showData($id)
    {
        $user = Auth::user();
        if (!$user) {
            return response()->json(['message' => 'Necesitas loguearte', 'code' => 404]);
        }
        $requests = Requests::where('id', $id)->get();
        if ($requests->isEmpty()) {
            return response()->json(['message' => 'No existe la solicitud', 'code' => 202]);
        }
        DB::beginTransaction();
        $child = $this->getBeneficiariesWithoutEconomic($id);
        $parents = $this->getBeneficiariesWithEconomic($id);
        $housing = $this->getHousingRequests($id);
        $references = $this->getReferencesRequests($id);
        $documents = $this->getDocument($id);
        DB::commit();
        return response()->json(['child' => $child, 'parents' => $parents, 'housing' => $housing, 'references' => $references, 'documents' => $documents]);
    }

    protected function getBeneficiariesWithoutEconomic($id)
    {
        return Beneficiary::whereDoesntHave('economic')->whereHas('requests', function ($query) use ($id) {
            $query->where('requests.id', $id);
        })->first();
    }

    protected function getDocument($id)
    {
        return RequestDocument::where('request_id', $id)->get();
    }

    protected function getBeneficiariesWithEconomic($id)
    {
        return Beneficiary::has('economic')->whereHas('requests', function ($query) use ($id) {
            $query->where('requests.id', $id);
        })->with('economic', 'employee', 'address')->get();
    }

    protected function getHousingRequests($id)
    {
        return HousingRequest::with('address')->where('request_id', $id)->first();
    }

    protected function getReferencesRequests($id)
    {
        return ReferencesRequest::where('request_id', $id)->get();
    }

    public function storeReference(Request $request)
    {
        $user = Auth::user();
        if (!$user) {
            $response['message'] = "Necesitas loguearte";
            $response['code'] = 404;
            return response()->json($response);
        }

        $requests = Requests::where('id', $request->request_id)->where('user_id', $user->id)->first();

        if ($requests !== null) {

            if ($requests->procedure_id == 1) {
                $limit = 2;
            } else {
                $limit = 3;
            }

            DB::beginTransaction();
            try {
                ReferencesRequest::create($request->all());
                $count = ReferencesRequest::where('request_id', $request->request_id)->count();

                if ($count == $limit) {
                    $requests->update([
                        'status_request_id' => 7
                    ]);

                    Log::create([
                        'user_id' => auth()->id(), // o null si el usuario no está autenticado
                        'receiver_id' => auth()->id(),
                        'request_id' => $requests->id,
                        'action' => 'Referencias personales creadas',
                        'description' => 'Creación de referencias personales del usuario para la solicitud ' . $requests->invoice . '. Y solicitud cumpletada.',
                        'status' => 1,
                        'read' => 0,
                    ]);
                }

                DB::commit();

                $response['message'] = "Referencia personal registradas correctamente";
                $response['code'] = 200;
            } catch (\Throwable $th) {
                $response['message'] = "Referencia personal no registrada";
                $response['code'] = 202;
            }
        } else {
            $response['message'] = "No existe la solicitud";
            $response['code'] = 202;
        }

        return response()->json($response);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $user = Auth::user();
        $year = Carbon::now()->format('Y');

        if (!$user) {
            $response['message'] = "Necesitas loguearte";
            $response['code'] = 404;
            return response()->json($response);
        }

        $requests = Requests::where('procedure_id', $request->procedure_id)->where('user_id', $user->id)->where('status_request_id', 1)->get();

        if ($requests->isEmpty()) {
            DB::beginTransaction();
            try {
                if ($request->procedure_id < 10) {
                    $number = Requests::select('invoice')->where('invoice', 'like', $year . '0' . $request->procedure_id  . '%')->whereYear('created_at', $year)->orderBy('invoice', 'desc')->first();
                } else {
                    $number = Requests::select('invoice')->where('invoice', 'like', $year . $request->procedure_id . '%')->whereYear('created_at', $year)->orderBy('invoice', 'desc')->first();
                }

                $folio = $number->invoice ?? null;
                if ($folio == null) {
                    if ($request->procedure_id < 10) {
                        $folio = intval($year . '0' . $request->procedure_id . '0001');
                    } else {
                        $folio = intval($year . $request->procedure_id  . '0001');
                    }
                } else {
                    $folio = $folio + 1;
                }

                $newRequest = Requests::create([
                    'invoice' => $folio,
                    'procedure_id' => $request->procedure_id,
                    'user_id' => $user->id,
                    'center_id' => $request->center_id,
                    'status_request_id' => 1,
                    'priority_id' => 1
                ]);

                if ($request->procedure_id == 1 || $request->procedure_id == 2) {
                    CrecheRequest::create([
                        'request_id' => $newRequest->id,
                        'degree_id' => $request->degree_id ?? null
                    ]);
                }
                $procedure = Procedure::find($request->procedure_id);
                if ($request->procedure_id == 1) {
                    BeneficiaryEmployee::create([
                        'employee_number' => $request->employee_number,
                        'job_position' => $request->job_position,
                        'dependence_id' => $request->dependence_id,
                        'request_id' => $newRequest->id
                    ]);

                    Log::create([
                        'user_id' => auth()->id(), // o null si el usuario no está autenticado
                        // 'receiver_id' => null,
                        'request_id' => $newRequest->id,
                        'action' => 'Informacion de empleado creada',
                        'description' => 'Información de empleado creada para el trámite de ' . $procedure->name . ' con folio de solicitud No.' . $folio . '.',
                        'status' => 0,
                        'read' => 0
                    ]);
                }



                Log::create([
                    'user_id' => auth()->id(), // o null si el usuario no está autenticado
                    'receiver_id' => auth()->id(),
                    'request_id' => $newRequest->id,
                    'action' => 'Solicitud creada',
                    'description' => 'Solicitud creada para el trámite de ' . $procedure->name . ' con folio de solicitud No.' . $folio . '.',
                    'status' => 0,
                    'read' => 0
                ]);

                DB::commit();

                $response['message'] = "Tramite iniciado";
                $response['code'] = 200;
            } catch (\Throwable $th) {
                DB::rollBack();
                $response['message'] = "El tramite no se ha iniciado";
                $response['code'] = 202;
            }
        } else {
            $response['message'] = "Ya hiciste una solicitud para ese tramite";
            $response['code'] = 202;
        }

        return response()->json($response);
    }


    public function changeStatus(Request $request)
    {
        $query = Requests::find($request->id);
        if ($request->status_request_id == 2) {
            $query->update([
                'status_request_id' => $request->status_request_id
            ]);
            $query->update([
                'finished' => Carbon::now()
            ]);

            // Enviar el correo electrónico utilizando la vista personalizada
            $email = $query->user->email;
            Mail::send('emails.send-request', [
                'name' => $query->user->name,
                'request' => $query
            ], function (Message $message) use ($email) {
                $message->to($email)
                    ->subject('Solicitud Enviada');
            });

            //LOG PENDIENTE

            $response['code'] = 200;
            $response['message'] = "Solicitud terminada, sera revisada por los encargados de cada tramite, las actualizaciones importantes del estado de la solicituda llegaran al correo registrado.";
            return response()->json($response);
        } else if ($request->status_request_id == 6) {
            if ($query->status_request_id == 3) {
                $response['code'] = 200;
                $response['message'] = "La solicitud no se puede cancelar cuando ya fue aceptada.";
            }
            $query->update([
                'status_request_id' => $request->status_request_id
            ]);
            if ($query->status_request_id != 1) {
                $query->update([
                    'finished' => null
                ]);
            }

            //LOG PENDIENTE

            // Enviar el correo electrónico utilizando la vista personalizada
            $email = $query->user->email;
            $label = 'Solicitud No.' . $query->invoice . ' ha sido cancelada, los administradores del centro ' . $query->center_id . ' no podran ver mas tu solicitud.';
            $response['code'] = 200;
            $response['message'] = "Solicitud cancelada, los encargados del tramite no podran ver tu solicitud.";
        } else {
            $query->update([
                'status_request_id' => $request->status_request_id
            ]);
            $query->update([
                'finished' => null
            ]);
            // Enviar el correo electrónico utilizando la vista personalizada
            $email = $query->user->email;
            $label = 'Solicitud No.' . $query->invoice . ' ha sido actualizada, los administradores del centro ' . $query->center_id . ' han cambiado el estado a ' . $query->status_request_id . '.';
            $response['code'] = 200;
            $response['message'] = "Actualizacion exitosa";
        }

        Mail::send('emails.request-status-change', [
            'name' => $query->user->name,
            'request' => $query,
            'label' => $label
        ], function (Message $message) use ($email) {
            $message->to($email)
                ->subject('Estado De Solicitud Actualizado');
        });

        Log::create([
            'user_id' => auth()->id(), // o null si el usuario no está autenticado
            'request_id' => $query->id,
            'action' => 'Solicitud actualizada',
            'description' => $label,
            'status' => 0,
            'read' => 0
        ]);

        return response()->json($response);
    }

    /**
     * Display the specified resource.
     */
    // public function show(Requests $requests)
    // {
    //     $user = Auth::user();
    //     $model = Requests::query();
    //     $model->where('procedure_id', $user->department_id)->where('status_request_id', '<>', 1)->where('status_request_id', '<>', 6);;
    //     $model = $model->with('crecheRequest.degree');
    //     $model = $model->where('center_id', $user->center_id);
    //     ($user->department_id == 1 ? $model->has('crecheRequest') : null);
    //     $query = $model->has('beneficiaries')->orderBy('id', 'asc')->with(['beneficiaries' => function ($query) {
    //         $query->orderBy('edad', 'asc');
    //     }])->with('priority')->withCount('quotes')->paginate(10);
    //     return response()->json($query);
    // }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Requests $requests)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    // public function update(Request $request)
    // {
    //     $query = Requests::find($request->id);
    //     $query->update([
    //         'status_request_id' => $request->status_request_id
    //     ]);

    //     // Enviar el correo electrónico utilizando la vista personalizada
    //     // Mail::send('emails.reset-password', [
    //     //     'name' => $name,
    //     //     'resetUrl' => $resetUrl
    //     // ], function (Message $message) use ($email) {
    //     //     $message->to($email)
    //     //         ->subject('Restablecimiento de Contraseña');
    //     // });

    //     $response['code'] = 200;
    //     $response['message'] = "Actualizacion exitosa";
    //     return response()->json($response);
    // }

    public function updateStatus(Request $request)
    {
        $query = Requests::with('user')->where('id', $request->id)->first();

        $statusAnterior = $query->status_request->name;

        $query->update([
            'status_request_id' => $request->status_request_id
        ]);

        $statusMessages = [
            2 => "Solicitud en Proceso.",
            3 => "Solicitud Aceptada",
            4 => "Solicitud Rechazada",
        ];

        $response = [
            'code' => 200,
            'message' => isset($statusMessages[$request->status_request_id])
                ? "Estatus Actualizado: " . $statusMessages[$request->status_request_id]
                : "Actualización exitosa"
        ];

        $email = $query->user->email;

        $newStatus = StatusRequest::find($request->status_request_id);


        $label = 'Solicitud No.' . $query->id . ' ha sido actualizada, los administradores del centro "' . $query->center->name . '" han cambiado el estado de "' . $statusAnterior . '" a "' . $newStatus->name . '".';

        Mail::send('emails.request-status-change', [
            'name' => $query->user->name,
            'request' => $query,
            'label' => $label
        ], function (Message $message) use ($email) {
            $message->to($email)
                ->subject('Estado De Solicitud Actualizado');
        });

        Log::create([
            'user_id' => auth()->id(), // o null si el usuario no está autenticado
            'receiver_id' => $query->user->id,
            'request_id' => $query->id,
            'action' => 'Solicitud actualizada',
            'description' => 'Solicitud con folio No.' . $query->invoice . ' actualizada, nuevo estado de solicitud ' . $newStatus->name . '.',
            'status' => 1,
            'read' => 0
        ]);

        return response()->json($response);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Requests $requests)
    {
        //
    }

    public function changeCenter(Request $request)
    {

        // $query = Requests::with('center','crecheRequest')->where('id', $request->id)->first();

        $query = DB::table('requests')->join('creche_requests as cr', 'cr.request_id', '=', 'requests.id')->where('requests.id', $request->id)->first();
        $centerAnterior = $query;

        $crcheRequest = CrecheRequest::where('id', $centerAnterior->id)->first();

        $crcheRequest->update([
            'center_id' => $request->center_id,
            'degree_id' => $request->degree_id
        ]);

        $response = [
            'code' => 200,
            'message' => "Solicitud cambiada de centro exitosamente"
        ];

        Requests::find($request->id)->update(['center_id' => $request->center_id]);
        $request = Requests::with('user')->where('id', $request->id)->first();
        $center = Center::where('id', $request->center_id)->first();
        $email = $request->user->email;

        $label = 'La solicitud No.' . $request->invoice . ' ha sido actualizada, los administradores han cambiado el centro de destino de la solicitud a "' . $center->name . '".';

        // Mail::send('emails.change-center', [
        //     'name' => $query->user->name,
        //     'center' => $center,
        //     'label' => $label
        // ], function (Message $message) use ($email) {
        //     $message->to($email)
        //         ->subject('Estado De Solicitud Actualizado');
        // });

        return response()->json($response);
    }

    public function search($value)
    {
        $user = Auth::user();

        $param = explode(":", $value);

        // Usuarios con rol 1 no pueden consultar
        if ($user->role_id == 1) {
            return;
        }

        $model = Requests::query();

        // Roles admin centro y admin trámite solo pueden ver solicitudes de su trámite en específico
        if ($user->role_id == 2 || $user->role_id == 3) {
            $model->where('procedure_id', $user->department_id);
        }

        // No se muestran las solicitudes sin terminar o canceladas
        $model->whereNotIn('status_request_id', [1, 6]);

        // Mostrar las solicitudes de modificación
        $model->with('modify_forms');

        // Admin de centro, solo ver solicitudes de su centro
        if ($user->role_id == 2) {
            $model->where('center_id', $user->center_id);
        }

        // Si el usuario corresponde a NIDOS, la solicitud debe de estar aplicando a una guardería
        if ($user->department_id == 1 || $user->department_id == 2) {
            $model->has('crecheRequest')->with('crecheRequest.degree');
        }

        ($param[0] == 'status' ? $model->where('status_request_id', $param[1]) : null);
        // Las solicitudes deben de tener beneficiarios y se ordenan por edad los beneficiarios
        $model->has('beneficiaries')->orderBy('id', 'asc')->with(['beneficiaries' => function ($query) {
            $query->orderBy('id', 'asc');
        }])->with('priority');

        if ($param[0] == 'quote') {
            ($param[1] == 1 ? $model->whereHas('quotes', function ($query) {
                $query->whereNotNull('id');
            }) : $model->doesntHave('quotes'));
        }

        $query = $model->withCount('quotes')->paginate(10);

        return response()->json($query);
    }

    public function searchValue($value)
    {
        $user = Auth::user();

        // Usuarios con rol 1 no pueden consultar
        if ($user->role_id == 1) {
            return;
        }

        $model = Requests::query();

        // Roles admin centro y admin trámite solo pueden ver solicitudes de su trámite en específico
        if ($user->role_id == 2 || $user->role_id == 3) {
            $model->where('procedure_id', $user->department_id);
        }

        // No se muestran las solicitudes sin terminar o canceladas
        $model->whereNotIn('status_request_id', [1, 6]);

        // Mostrar las solicitudes de modificación
        $model->with('modify_forms');

        // Admin de centro, solo ver solicitudes de su centro
        if ($user->role_id == 2) {
            $model->where('center_id', $user->center_id);
        }

        // Si el usuario corresponde a NIDOS, la solicitud debe de estar aplicando a una guardería
        if ($user->department_id == 1 || $user->department_id == 2) {
            $model->has('crecheRequest')->with('crecheRequest.degree');
        }

        $model->has('beneficiaries')->orderBy('id', 'asc')->with(['beneficiaries' => function ($query) {
            $query->orderBy('id', 'asc');
        }])->with('priority');

        //REALIZAR BUSQUEDA 
        $query = $model->where(function ($query) use ($value) {
            $query->where('invoice', 'like', '%' . $value . '%')
                ->orWhereHas('beneficiaries', function ($query) use ($value) {
                    $query->whereRaw("CONCAT(nombre, ' ', apaterno, ' ', amaterno) LIKE ?", ['%' . $value . '%']);
                });
        })
            ->withCount('quotes')
            ->paginate(10);

        return response()->json($query);
    }
}
