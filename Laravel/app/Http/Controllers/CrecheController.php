<?php

namespace App\Http\Controllers;

use App\Models\Beneficiary;
use App\Models\Creche;
use App\Models\Degree;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class CrecheController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $user = Auth::user();
        $model = Creche::query();
        $query = $model //->has('')
            ->orderBy('degree_id', 'asc')->where('center_id', $user->center_id)
            ->withCount(['beneficiaryCreche as beneficiry_count' => function ($query) {
                return $query->where('status', 1);
            }, 'requests as process' => function ($query) use ($user) {
                return $query->where('status_request_id', 2)->where('procedure_id', $user->department_id);
            }, 'requests as refused' => function ($query) use ($user) {
                return $query->where('status_request_id', 4)->where('procedure_id', $user->department_id);
            }])->with('degree', 'room')->paginate();

        $processCounts = Degree::leftJoin('creche_requests as cr', function ($join) use ($user) {
            $join->on('degrees.id', '=', 'cr.degree_id')
                ->leftJoin('requests as r', function ($joinRequests) use ($user) {
                    $joinRequests->on('cr.request_id', '=', 'r.id')
                        ->where('r.center_id', $user->center_id)
                        ->where('r.status_request_id', 2)
                        ->where('r.procedure_id', $user->department_id);
                });
        })
            ->groupBy('degrees.id', 'degrees.name')
            ->select('degrees.name', DB::raw('COALESCE(COUNT(r.id), 0) as process_count'))
            ->get();

        $refusedCounts = Degree::leftJoin('creche_requests as cr', function ($join) use ($user) {
            $join->on('degrees.id', '=', 'cr.degree_id')
                ->leftJoin('requests as r', function ($joinRequests) use ($user) {
                    $joinRequests->on('cr.request_id', '=', 'r.id')
                        ->where('r.center_id', $user->center_id)
                        ->where('r.status_request_id', 4)
                        ->where('r.procedure_id', $user->department_id);
                });
        })
            ->groupBy('degrees.id', 'degrees.name')
            ->select('degrees.name', DB::raw('COALESCE(COUNT(r.id), 0) as refused_count'))
            ->get();

        $beneficiaryCounts = Degree::leftJoin('creches as c', 'degrees.id', '=', 'c.degree_id')
            ->leftJoin('beneficiary_creches as b', function ($joinBeneficiaries) {
                $joinBeneficiaries->on('c.id', '=', 'b.creche_id')
                    ->where('b.status', 1);
            })
            ->where('c.center_id', $user->center_id)
            ->groupBy('degrees.id', 'degrees.name')
            ->select('degrees.name', DB::raw('COALESCE(COUNT(b.id), 0) as beneficiary_count'))
            ->get();


        $capacitySums = Degree::leftJoin('creches as c', 'degrees.id', '=', 'c.degree_id')
            ->where('c.center_id', $user->center_id)
            ->groupBy('degrees.id', 'degrees.name')
            ->select('degrees.name', DB::raw('COALESCE(SUM(c.capacity), 0) as capacity_sum'))
            ->get();

        return response()->json([
            'creches' => $query->toArray(), // Convertir el objeto $query a un arreglo
            'degree' => [
                'process' => $processCounts,
                'refuse' => $refusedCounts,
                'benediciaries' => $beneficiaryCounts,
                'capacity' => $capacitySums
            ]
        ]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function showCreche($center)
    {
        $model = Creche::query();
        $query = $model
            ->orderBy('room_id', 'asc')->where('center_id', $center)
            ->withCount(['beneficiaryCreche as beneficiry_count' => function ($query) {
                return $query->where('status', 1);
            }])->with(['degree', 'room'])->get();
        return response()->json($query);
    }

    public function showBeneficiaryCreche($creche)
    {
        $model = Beneficiary::query();
        $query = $model->has('beneficiaryCreche')->whereHas('beneficiaryCreche', function ($query) use ($creche) {
            $query->where('creche_id', $creche)->where('status', 1);
        })->with('beneficiaryCreche')->get();
        return response()->json($query);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(Creche $creche)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Creche $creche)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Creche $creche)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Creche $creche)
    {
        //
    }
}
